# rdsim

RDSim allows to instantiate a set of nodes and links with different random variables depending on the targeted scenario, and generate the corresponding DODAG following different routing approaches to manage multiple interfaces. Thus it allows a first evaluation to compare the three different approaches from a topology point of view. It also allows to visualize the resulting topologies with a graphical representation. We choose to develop this new tool because other commonly used simulation tools (e.g., Cooja, NS-3) are quite complex and we wanted to have a first idea of resulting DODAG following three different approaches without the complexity of link abstraction and message transmission simulation. 

RDSim allows to generate a random topology with a specific number of nodes and a specific proportion of hybrid/single interface node. It can also load a predefined topology. Nodes in the topology are the vertices of the graph and all edges have a random computed weight (i.e which could represent a link quality metric between 1 and 7, where 7 is the worst value).

All nodes except the root have a rank attribute set to 65535 during the creation of the graph. The rank attribute of the root is fixed to 256 for both technologies, because in the MI solution, a node has two ranks. The resulting graph, considered as the physical representation of the hybrid network is analyzed iteratively from the root to the next hops to construct the DAG. During this process, node ranks are calculated.

It has to be noted that RDSim does not handle RPL message exchanges and application traffic, its purpose is the graph generation and performance analysis on generated topologies.

```
François Lemercier. Multiple interface management in smart grid networks. Networking and Internet Architecture [cs.NI]. Ecole nationale supérieure Mines-Télécom Atlantique, 2018. English. ⟨NNT : 2018IMTA0100⟩. ⟨tel-02095994⟩
```

https://tel.archives-ouvertes.fr/tel-02095994
