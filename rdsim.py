# -*-coding:Utf-8 -*


import warnings

warnings.filterwarnings("ignore", module="matplotlib")
from datetime import datetime
import networkx as nx
import pygraphviz as pgv
import numpy as np
import matplotlib.pyplot as plt
import pylab
import sys
import csv
import copy



from collections import Counter




DAG_PLC = nx.Graph()
DAG_RF = nx.Graph()
DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
graph_user = nx.MultiGraph() # Undirected graph creation
#node_number = input('Choose a number of nodes for your graph greater than 3: ')

while True:
     try:
         node_number1 = raw_input("Choose a number of nodes for your graph greater than 3: ")
         n = float(node_number1)
         if n > 3:
             node_number = int(node_number1)
             break
     except ValueError:
         continue

while True:
     try:
         drawing1 = raw_input("Do you want to have a graphic output of the graphs? y/n ")
         if drawing1 == 'n' or drawing1 == 'y':
             drawing = drawing1
             break
     except ValueError:
         continue

##while True:
##     try:
##         sim_number1 = raw_input("Number of simulation pass: ")
##         n = float(sim_number1)
##         if n > 0:
##             sim_number = int(sim_number1)
##             break
##     except ValueError:
##         continue
     
#policy = input('Choose a policy: \n1- User PLC \n2- User RF \n3- Best link \n4- Last MAC used\nPolicy? ')

##while True:
##     try:
##         policy1 = raw_input("Choose a policy: \n1- User PLC \n2- User RF \n3- Best link \nPolicy? ")
##         n = float(policy1)
##         if n == 3 or n == 1 or n == 2:
##             policy = int(policy1)
##             break
##     except ValueError:
##         continue

policy = 3

def namestr(obj, namespace):
        return [name for name in namespace if namespace[name] is obj]
     
def graph_random(nb):

        edge_density = (nb*4)  #*2
        n = 0
        n2 = 0
        a = nx.Graph()
        b = nx.Graph()
        a.add_node('Root', pos=(5, 13), rank = 256)
        b.add_node('Root', pos=(5, 13), rank = 256)
        for i in range (nb-1):
                a.add_node(str(i+1), pos=(5, 13), rank = 65536)
        for i in range (nb-1):
                b.add_node(str(i+1), pos=(5, 13), rank = 65536)

                
	
        for k in range ((nb/2)): #nb/4
                        
                r= int(round(np.random.uniform(1,8), 0))
                test_edges = a.edges()
                n = np.random.random_integers(nb-2)
                e = ('Root', str(n))
                if (e in test_edges):
                        k=k-1
                else:
                        a.add_edge('Root', str(n), key = k, color = 'Blue', etx=r, mac = 'RF')

        for m in range ((nb/2)): #nb/4
                        
                r= int(round(np.random.uniform(1,8), 0))
                test_edges = b.edges()
                n = np.random.random_integers(nb-2)
                e = ('Root', str(n))
                if (e in test_edges):
                        m=m-1
                else:
                        b.add_edge('Root', str(n), key = m+k, color = 'Red', etx=r, mac = 'PLC')
                                

        for j in range ((edge_density)):
                r= int(round(np.random.uniform(1,8), 0))
                test_edges = a.edges()
                while(n == n2 ):
                        n = np.random.random_integers(nb-2)
    
                        n2 = np.random.random_integers(nb-2)
                #graph_user.add_edge(str(n+1), str(n2+1), color = color, etx=r, mac = mac)
                e = (str(n+1), str(n2+1))
                f = (str(n2+1), str(n+1))
                #if (graph_user.get_edge_data(str(n+1), str(n2+1), default = 0)) == 0:
                if (e in test_edges):
                        j=j-1
                elif (f in test_edges):
                        j=j-1

                else:
                        a.add_edge(str(n+1), str(n2+1), key = m+k+j, color = 'Blue', etx=r, mac = 'RF')

                        
                n = 0
                n2 = 0
        for l in range ((edge_density)):
                r= int(round(np.random.uniform(1,8), 0))
                test_edges = b.edges()
                while(n == n2 ):
                        n = np.random.random_integers(nb-2)
    
                        n2 = np.random.random_integers(nb-2)
                #graph_user.add_edge(str(n+1), str(n2+1), color = color, etx=r, mac = mac)
                e = (str(n+1), str(n2+1))
                f = (str(n2+1), str(n+1))
                #if (graph_user.get_edge_data(str(n+1), str(n2+1), default = 0)) == 0:
                if (e in test_edges):
                        j=j-1
                elif (f in test_edges):
                        j=j-1

                else:
                        b.add_edge(str(n+1), str(n2+1), key = l+m+k+j, color = 'Red', etx=r, mac = 'PLC')

                        
                n = 0
                n2 = 0


##################################
                ##################################
                ##################################
                ##################################
##################################
def graph_random_2(nb):
        w=0
        x=0
        u=0
        v=0
        s=0
        t=0
        p=0
        q=0
        m=0
        o=0
        l=0
        k=0
        edge_density = (nb*4)  #*2
        n = 0
        n2 = 0
        a = nx.Graph()
        b = nx.Graph()

        ####### Node creation and rank definition
        a.add_node('Root', pos=(5, 13), rank = 256)
        b.add_node('Root', pos=(5, 13), rank = 256)
        for i in range (nb-1):
                a.add_node(str(i+1), pos=(5, 13), rank = 65536)
        for i in range (nb-1):
                b.add_node(str(i+1), pos=(5, 13), rank = 65536)
                
	#######1st Wave edges creation RF
        first_wave = []
        w1_RF= int(round(np.random.uniform(5,10), 0))#5,10
        for k in range (w1_RF): #
                        
                r= int(round(np.random.uniform(1,8), 0))
                test_edges = a.edges()
                e = ('Root', str(k+1))
                a.add_edge('Root', str(k+1), key = k+1, color = 'Blue', etx=r, mac = 'RF')
                first_wave.append(str(k+1))

        for l in range(len(first_wave)/2):
                       r= int(round(np.random.uniform(1,8), 0))
                       test_edges = a.edges()
                       n= int(round(np.random.uniform(1,len(first_wave)/2), 0))
                       e = (first_wave[n], str(l+1))
                       f = (str(l+1), first_wave[n])
                       
                       while (e in test_edges or f in test_edges or first_wave[n] == str(l+1)):
                            n= int(round(np.random.uniform(1,len(first_wave)/2), 0))
                            e = (first_wave[n], str(l+1))
                            f = (str(l+1), first_wave[n])
                            
                       a.add_edge(first_wave[n], str(l+1), key = l+k+2, color = 'Blue', etx=r, mac = 'RF')
        #######2nd Wave edges creation RF
        if len(a.edges()) < nb*4:
             second_wave = []
             w2_RF= int(round(np.random.uniform(15,25), 0))  #15,25
             for m in range (w2_RF): #
                             
                     r= int(round(np.random.uniform(1,8), 0))
                     r_w1_rf = int(round(np.random.uniform(0,w1_RF-1), 0))
                     test_edges = a.edges()
                     e = (str(first_wave[r_w1_rf]), str(w1_RF+m+1))
                     f = (str(w1_RF+m+1) , str(first_wave[r_w1_rf]))
                     while (e in test_edges or f in test_edges or str(first_wave[r_w1_rf]) == str(w1_RF+m+1)):
                          r= int(round(np.random.uniform(1,8), 0))
                          r_w1_rf = int(round(np.random.uniform(0,w1_RF-1), 0))
                          e = (str(first_wave[r_w1_rf]), str(w1_RF+m+1))
                          f = (str(w1_RF+m+1) , str(first_wave[r_w1_rf]))

                     a.add_edge(first_wave[r_w1_rf], str(w1_RF+m+1), key = m+l+k+3, color = 'Blue', etx=r, mac = 'RF')
                     second_wave.append(str(w1_RF+m+1))

             for o in range(len(second_wave)/2):
                         r= int(round(np.random.uniform(1,8), 0))
                         test_edges = a.edges()
                         n= int(round(np.random.uniform(1,len(second_wave)/2), 0))
                         n2 = int(round(np.random.uniform(1,len(second_wave)/2), 0))
                         e = (second_wave[n], second_wave[n2])
                         f = (second_wave[n2], second_wave[n])
                            
                         while (e in test_edges or f in test_edges or second_wave[n2] == second_wave[n]):
                              n= int(round(np.random.uniform(1,len(second_wave)/2), 0))
                              n2 = int(round(np.random.uniform(1,len(second_wave)/2), 0))
                              
                              e = (second_wave[n], second_wave[n2])
                              f = (second_wave[n2], second_wave[n])
                                
                         a.add_edge(second_wave[n], second_wave[n2], key = m+o+l+k+4, color = 'Blue', etx=r, mac = 'RF')
        #######3rd Wave edges creation RF
        if len(a.edges()) < nb*4:
             third_wave = []
             w3_RF= int(round(np.random.uniform(40,70), 0))#40,70
             for p in range (w3_RF): #
                             
                     r= int(round(np.random.uniform(1,8), 0))
                     r_w2_rf = int(round(np.random.uniform(0,w2_RF-1), 0))
                     test_edges = a.edges()
                     #n = np.random.random_integers(nb-2)
                     e = (str(second_wave[r_w2_rf]), str(w1_RF+w2_RF+p+1))
                     f = (str(w1_RF+w2_RF+p+1) , str(second_wave[r_w2_rf]))
                     while (e in test_edges or f in test_edges or str(second_wave[r_w2_rf]) == str(w1_RF+w2_RF+p+1)):
                          r= int(round(np.random.uniform(1,8), 0))
                          r_w2_rf = int(round(np.random.uniform(0,w2_RF-1), 0))
                          e = (str(second_wave[r_w2_rf]), str(w1_RF+w2_RF+p+1))
                          f = (str(w1_RF+w2_RF+p+1) , str(second_wave[r_w2_rf]))

                     a.add_edge(second_wave[r_w2_rf], str(w1_RF+w2_RF+p+1), key = p+o+m+l+k+5, color = 'Blue', etx=r, mac = 'RF')
                     third_wave.append(str(w1_RF+w2_RF+p+1))

             for q in range(len(third_wave)/2):
                         r= int(round(np.random.uniform(1,8), 0))
                         test_edges = a.edges()
                         n= int(round(np.random.uniform(1,len(third_wave)/2), 0))
                         n2 = int(round(np.random.uniform(1,len(third_wave)/2), 0))
                         e = (third_wave[n], third_wave[n2])
                         f = (third_wave[n2], third_wave[n])
                            
                         while (e in test_edges or f in test_edges or third_wave[n2] == third_wave[n]):
                              n= int(round(np.random.uniform(1,len(third_wave)/2), 0))
                              n2 = int(round(np.random.uniform(1,len(third_wave)/2), 0))
                              
                              e = (third_wave[n], third_wave[n2])
                              f = (third_wave[n2], third_wave[n])
                                                        
                         a.add_edge(third_wave[n], third_wave[n2], key = p+q+m+o+l+k+6, color = 'Blue', etx=r, mac = 'RF')
        #######4th Wave edges creation RF
        if len(a.edges()) < nb*4:
             fourth_wave = []
             w4_RF= int(round(np.random.uniform(100,200), 0))#100,200
             for s in range (w4_RF): #
                             
                     r= int(round(np.random.uniform(1,8), 0))
                     r_w3_rf = int(round(np.random.uniform(0,w3_RF-1), 0))
                     test_edges = a.edges()
                     e = (str(third_wave[r_w3_rf]), str(w1_RF+w2_RF+w3_RF+s+1))
                     f = (str(w1_RF+w2_RF+w3_RF+s+1) , str(third_wave[r_w3_rf]))
                     while (e in test_edges or f in test_edges or str(third_wave[r_w3_rf]) == str(w1_RF+w2_RF+w3_RF+s+1)):
                          r= int(round(np.random.uniform(1,8), 0))
                          r_w3_rf = int(round(np.random.uniform(0,w3_RF-1), 0))
                          e = (str(third_wave[r_w3_rf]), str(w1_RF+w2_RF+w3_RF+s+1))
                          f = (str(w1_RF+w2_RF+w3_RF+s+1) , str(third_wave[r_w3_rf]))

                     a.add_edge(third_wave[r_w3_rf], str(w1_RF+w2_RF+w3_RF+s+1), key = s+q+p+o+m+l+k+7, color = 'Blue', etx=r, mac = 'RF')
                     fourth_wave.append(str(w1_RF+w2_RF+w3_RF+s+1))

             for t in range(len(fourth_wave)/2):
                         r= int(round(np.random.uniform(1,8), 0))
                         test_edges = a.edges()
                         n= int(round(np.random.uniform(1,len(fourth_wave)/2), 0))
                         n2 = int(round(np.random.uniform(1,len(fourth_wave)/2), 0))
                         e = (fourth_wave[n], fourth_wave[n2])
                         f = (fourth_wave[n2], fourth_wave[n])
                            
                         while (e in test_edges or f in test_edges or fourth_wave[n2] == fourth_wave[n]):
                              n= int(round(np.random.uniform(1,len(fourth_wave)/2), 0))
                              n2 = int(round(np.random.uniform(1,len(fourth_wave)/2), 0))
                              
                              e = (fourth_wave[n], fourth_wave[n2])
                              f = (fourth_wave[n2], fourth_wave[n])
                            
                         a.add_edge(fourth_wave[n], fourth_wave[n2], key = s+t+p+q+m+o+l+k+8, color = 'Blue', etx=r, mac = 'RF')
        #######5th Wave edges creation RF
        if len(a.edges()) < nb*4:
             fifth_wave = []
             w5_RF= int(round(np.random.uniform(500,1000), 0))#500,1000
             for u in range (w5_RF): #
                             
                     r= int(round(np.random.uniform(1,8), 0))
                     r_w4_rf = int(round(np.random.uniform(0,w4_RF-1), 0))
                     test_edges = a.edges()
                     e = (str(fourth_wave[r_w4_rf]), str(w1_RF+w2_RF+w3_RF+w4_RF+u+1))
                     f = (str(w1_RF+w2_RF+w3_RF+w4_RF+u+1) , str(fourth_wave[r_w4_rf]))
                     while (e in test_edges or f in test_edges or str(fourth_wave[r_w4_rf]) == str(w1_RF+w2_RF+w3_RF+w4_RF+u+1)):
                          r= int(round(np.random.uniform(1,8), 0))
                          r_w4_rf = int(round(np.random.uniform(0,w4_RF-1), 0))
                          e = (str(fourth_wave[r_w4_rf]), str(w1_RF+w2_RF+w3_RF+w4_RF+u+1))
                          f = (str(w1_RF+w2_RF+w3_RF+w4_RF+u+1) , str(fourth_wave[r_w4_rf]))

                     a.add_edge(fourth_wave[r_w4_rf], str(w1_RF+w2_RF+w3_RF+w4_RF+u+1), key = u+t+s+q+p+o+m+l+k+9, color = 'Blue', etx=r, mac = 'RF')
                     fifth_wave.append(str(w1_RF+w2_RF+w3_RF+w4_RF+u+1))

             for v in range(len(fifth_wave)/2):
                         r= int(round(np.random.uniform(1,8), 0))
                         test_edges = a.edges()
                         n= int(round(np.random.uniform(1,len(fifth_wave)/2), 0))
                         n2 = int(round(np.random.uniform(1,len(fifth_wave)/2), 0))
                         e = (fifth_wave[n], fifth_wave[n2])
                         f = (fifth_wave[n2], fifth_wave[n])
                            
                         while (e in test_edges or f in test_edges or fifth_wave[n2] == fifth_wave[n]):
                              n= int(round(np.random.uniform(1,len(fifth_wave)/2), 0))
                              n2 = int(round(np.random.uniform(1,len(fifth_wave)/2), 0))
                              
                              e = (fifth_wave[n], fifth_wave[n2])
                              f = (fifth_wave[n2], fifth_wave[n])
                            
                         a.add_edge(fifth_wave[n], fifth_wave[n2], key = u+v+s+t+p+q+m+o+l+k+10, color = 'Blue', etx=r, mac = 'RF')

        #######6th Wave edges creation RF
        if len(a.edges()) < nb*4:
             sixth_wave = []
             w6_RF= int(round(np.random.uniform(1000,2000), 0))#1000,2000
             for w in range (w6_RF): #
                             
                     r= int(round(np.random.uniform(1,8), 0))
                     r_w5_rf = int(round(np.random.uniform(0,w5_RF-1), 0))
                     test_edges = a.edges()
                     e = (str(fifth_wave[r_w5_rf]), str(w1_RF+w2_RF+w3_RF+w4_RF+w5_RF+w+1))
                     f = (str(w1_RF+w2_RF+w3_RF+w4_RF+w5_RF+w+1) , str(fifth_wave[r_w5_rf]))
                     while (e in test_edges or f in test_edges or str(fifth_wave[r_w5_rf]) == str(w1_RF+w2_RF+w3_RF+w4_RF+w5_RF+w+1)):
                          r= int(round(np.random.uniform(1,8), 0))
                          r_w5_rf = int(round(np.random.uniform(0,w5_RF-1), 0))
                          e = (str(fifth_wave[r_w5_rf]), str(w1_RF+w2_RF+w3_RF+w4_RF+w5_RF+w+1))
                          f = (str(w1_RF+w2_RF+w3_RF+w4_RF+w5_RF+w+1) , str(fifth_wave[r_w5_rf]))

                     a.add_edge(fifth_wave[r_w5_rf], str(w1_RF+w2_RF+w3_RF+w4_RF+w5_RF+w+1), key = w+v+u+t+s+q+p+o+m+l+k+11, color = 'Blue', etx=r, mac = 'RF')
                     sixth_wave.append(str(w1_RF+w2_RF+w3_RF+w4_RF+w5_RF+w+1))

             for x in range(len(sixth_wave)/2):
                         r= int(round(np.random.uniform(1,8), 0))
                         test_edges = a.edges()
                         n= int(round(np.random.uniform(1,len(sixth_wave)/2), 0))
                         n2 = int(round(np.random.uniform(1,len(sixth_wave)/2), 0))
                         e = (sixth_wave[n], sixth_wave[n2])
                         f = (sixth_wave[n2], sixth_wave[n])
                            
                         while (e in test_edges or f in test_edges or sixth_wave[n2] == sixth_wave[n]):
                              n= int(round(np.random.uniform(1,len(sixth_wave)/2), 0))
                              n2 = int(round(np.random.uniform(1,len(sixth_wave)/2), 0))
                              
                              e = (sixth_wave[n], sixth_wave[n2])
                              f = (sixth_wave[n2], sixth_wave[n])
                            
                         a.add_edge(sixth_wave[n], sixth_wave[n2], key = w+x+u+v+s+t+p+q+m+o+l+k+12, color = 'Blue', etx=r, mac = 'RF')
        key_rf = w+x+u+v+s+t+p+q+m+o+l+k+12
        #print a.edges()
             


##################################
        
        ##################################
        ###########Graph PLC##############
        ##################################

##################################

        
        #######1st Wave edges creation PLC
        first_wave = []
        w1_PLC= int(round(np.random.uniform(5,10), 0)) #5,10
        for k in range (w1_PLC): #
                        
                r= int(round(np.random.uniform(1,8), 0))
                test_edges = b.edges()
                e = ('Root', str(k))
                b.add_edge('Root', str(k+1), key = key_rf+k+1, color = 'Red', etx=r, mac = 'PLC')
                first_wave.append(str(k+1))

        for l in range(len(first_wave)/2):
                       r= int(round(np.random.uniform(1,8), 0))
                       test_edges = b.edges()
                       n= int(round(np.random.uniform(1,len(first_wave)/2), 0))
                       e = (first_wave[n], str(l+1))
                       f = (str(l+1), first_wave[n])
                       
                       while (e in test_edges or f in test_edges or first_wave[n] == str(l+1)):
                            n= int(round(np.random.uniform(1,len(first_wave)/2), 0))
                            e = (first_wave[n], str(l+1))
                            f = (str(l+1), first_wave[n])
                       b.add_edge(first_wave[n], str(l+1), key = key_rf+l+k+2, color = 'Red', etx=r, mac = 'PLC')
        #######2nd Wave edges creation PLC
        if len(b.edges()) < nb*4:
             second_wave = []
             w2_PLC= int(round(np.random.uniform(15,25), 0))#15,25
             for m in range (w2_PLC): # Essai de w2_PLC à w2_PLC*2
                             
                     r= int(round(np.random.uniform(1,8), 0))
                     r_w1_plc = int(round(np.random.uniform(0,w1_PLC-1), 0))
                     test_edges = b.edges()
                     e = (str(first_wave[r_w1_plc]), str(w1_PLC+m+1))
                     f = (str(w1_PLC+m+1) , str(first_wave[r_w1_plc]))
                     while (e in test_edges or f in test_edges or str(first_wave[r_w1_plc]) == str(w1_PLC+m+1)):
                          r= int(round(np.random.uniform(1,8), 0))
                          r_w1_plc = int(round(np.random.uniform(0,w1_PLC-1), 0))
                          e = (str(first_wave[r_w1_plc]), str(w1_PLC+m+1))
                          f = (str(w1_PLC+m+1) , str(first_wave[r_w1_plc]))

                     b.add_edge(first_wave[r_w1_plc], str(w1_PLC+m+1), key = key_rf+m+l+k+3, color = 'Red', etx=r, mac = 'PLC')
                     second_wave.append(str(w1_PLC+m+1))

             for o in range(len(second_wave)/2):   # essai de SW/2 à SW
                         r= int(round(np.random.uniform(1,8), 0))
                         test_edges = b.edges()
                         n= int(round(np.random.uniform(1,len(second_wave)/2), 0))
                         n2 = int(round(np.random.uniform(1,len(second_wave)/2), 0))
                         e = (second_wave[n], second_wave[n2])
                         f = (second_wave[n2], second_wave[n])
                            
                         while (e in test_edges or f in test_edges or second_wave[n2] == second_wave[n]):
                              n= int(round(np.random.uniform(1,len(second_wave)/2), 0))
                              n2 = int(round(np.random.uniform(1,len(second_wave)/2), 0))
                              
                              e = (second_wave[n], second_wave[n2])
                              f = (second_wave[n2], second_wave[n])
                                
                         b.add_edge(second_wave[n], second_wave[n2], key = key_rf+m+o+l+k+4, color = 'Red', etx=r, mac = 'PLC')
        #######3rd Wave edges creation PLC
        if len(b.edges()) < nb*4:
             third_wave = []
             w3_PLC= int(round(np.random.uniform(40,70), 0))#40,70
             for p in range (w3_PLC): #
                             
                     r= int(round(np.random.uniform(1,8), 0))
                     r_w2_plc = int(round(np.random.uniform(0,w2_PLC-1), 0))
                     test_edges = b.edges()
                     e = (str(second_wave[r_w2_plc]), str(w1_PLC+w2_PLC+p+1))
                     f = (str(w1_PLC+w2_PLC+p+1) , str(second_wave[r_w2_plc]))
                     while (e in test_edges or f in test_edges or str(second_wave[r_w2_plc]) == str(w1_PLC+w2_PLC+p+1)):
                          r= int(round(np.random.uniform(1,8), 0))
                          r_w2_plc = int(round(np.random.uniform(0,(w2_PLC)-1), 0))
                          e = (str(second_wave[r_w2_plc]), str(w1_PLC+w2_PLC+p+1))
                          f = (str(w1_PLC+w2_PLC+p+1) , str(second_wave[r_w2_plc]))

                     b.add_edge(second_wave[r_w2_plc], str(w1_PLC+w2_PLC+p+1), key = key_rf+p+o+m+l+k+5, color = 'Red', etx=r, mac = 'PLC')
                     third_wave.append(str(w1_PLC+w2_PLC+p+1))

             for q in range(len(third_wave)/2):
                         r= int(round(np.random.uniform(1,8), 0))
                         test_edges = b.edges()
                         n= int(round(np.random.uniform(1,len(third_wave)/2), 0))
                         n2 = int(round(np.random.uniform(1,len(third_wave)/2), 0))
                         e = (third_wave[n], third_wave[n2])
                         f = (third_wave[n2], third_wave[n])
                            
                         while (e in test_edges or f in test_edges or third_wave[n2] == third_wave[n]):
                              n= int(round(np.random.uniform(1,len(third_wave)/2), 0))
                              n2 = int(round(np.random.uniform(1,len(third_wave)/2), 0))
                              e = (third_wave[n], third_wave[n2])
                              f = (third_wave[n2], third_wave[n])
                                 
                         b.add_edge(third_wave[n], third_wave[n2], key = key_rf + p+q+m+o+l+k+6, color = 'Red', etx=r, mac = 'PLC')
        #######4th Wave edges creation PLC
        if len(b.edges()) < nb*4:
             fourth_wave = []
             w4_PLC= int(round(np.random.uniform(100,200), 0))#100,200
             for s in range (w4_PLC): #
                             
                     r= int(round(np.random.uniform(1,8), 0))
                     r_w3_plc = int(round(np.random.uniform(0,w3_PLC-1), 0))
                     test_edges = b.edges()
                     e = (str(third_wave[r_w3_plc]), str(w1_PLC+w2_PLC+w3_PLC+s+1))
                     f = (str(w1_PLC+w2_PLC+w3_PLC+s+1) , str(third_wave[r_w3_plc]))
                     while (e in test_edges or f in test_edges or str(third_wave[r_w3_plc]) == str(w1_PLC+w2_PLC+w3_PLC+s+1)):
                          r= int(round(np.random.uniform(1,8), 0))
                          r_w3_plc = int(round(np.random.uniform(0,w3_PLC-1), 0))
                          e = (str(third_wave[r_w3_plc]), str(w1_PLC+w2_PLC+w3_PLC+s+1))
                          f = (str(w1_PLC+w2_PLC+w3_PLC+s+1) , str(third_wave[r_w3_plc]))

                     b.add_edge(third_wave[r_w3_plc], str(w1_PLC+w2_PLC+w3_PLC+s+1), key = key_rf+s+q+p+o+m+l+k+7, color = 'Red', etx=r, mac = 'PLC')
                     fourth_wave.append(str(w1_PLC+w2_PLC+w3_PLC+s+1))

             for t in range(len(fourth_wave)/2):
                         r= int(round(np.random.uniform(1,8), 0))
                         test_edges = b.edges()
                         n= int(round(np.random.uniform(1,len(fourth_wave)/2), 0))
                         n2 = int(round(np.random.uniform(1,len(fourth_wave)/2), 0))
                         e = (fourth_wave[n], fourth_wave[n2])
                         f = (fourth_wave[n2], fourth_wave[n])
                            
                         while (e in test_edges or f in test_edges or fourth_wave[n2] == fourth_wave[n]):
                              n= int(round(np.random.uniform(1,len(fourth_wave)/2), 0))
                              n2 = int(round(np.random.uniform(1,len(fourth_wave)/2), 0))
                              e = (fourth_wave[n], fourth_wave[n2])
                              f = (fourth_wave[n2], fourth_wave[n])
                                 
                         b.add_edge(fourth_wave[n], fourth_wave[n2], key = key_rf+s+t+p+q+m+o+l+k+8, color = 'Red', etx=r, mac = 'PLC')
                    
        #######5th Wave edges creation PLC
        if len(b.edges()) < nb*4:
             fifth_wave = []
             w5_PLC= int(round(np.random.uniform(500,1000), 0))#500,1000
             for u in range (w5_PLC): #
                             
                     r= int(round(np.random.uniform(1,8), 0))
                     r_w4_plc = int(round(np.random.uniform(0,w4_PLC-1), 0))
                     test_edges = b.edges()
                     e = (str(fourth_wave[r_w4_plc]), str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+u+1))
                     f = (str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+u+1) , str(fourth_wave[r_w4_plc]))
                     while (e in test_edges or f in test_edges or str(fourth_wave[r_w4_plc]) == str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+u+1)):
                          r= int(round(np.random.uniform(1,8), 0))
                          r_w4_plc = int(round(np.random.uniform(0,w4_PLC-1), 0))
                          e = (str(fourth_wave[r_w4_plc]), str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+u+1))
                          f = (str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+u+1) , str(fourth_wave[r_w4_plc]))

                     b.add_edge(fourth_wave[r_w4_plc], str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+u+1), key = key_rf+u+t+s+q+p+o+m+l+k+9, color = 'Red', etx=r, mac = 'PLC')
                     fifth_wave.append(str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+u+1))

             for v in range(len(fifth_wave)/2):
                         r= int(round(np.random.uniform(1,8), 0))
                         test_edges = b.edges()
                         n= int(round(np.random.uniform(1,len(fifth_wave)/2), 0))
                         n2 = int(round(np.random.uniform(1,len(fifth_wave)/2), 0))
                         e = (fifth_wave[n], fifth_wave[n2])
                         f = (fifth_wave[n2], fifth_wave[n])
                            
                         while (e in test_edges or f in test_edges or fifth_wave[n2] == fifth_wave[n]):
                              n= int(round(np.random.uniform(1,len(fifth_wave)/2), 0))
                              n2 = int(round(np.random.uniform(1,len(fifth_wave)/2), 0))
                              
                              e = (fifth_wave[n], fifth_wave[n2])
                              f = (fifth_wave[n2], fifth_wave[n])
                            
                         b.add_edge(fifth_wave[n], fifth_wave[n2], key = key_rf+u+v+s+t+p+q+m+o+l+k+10, color = 'Red', etx=r, mac = 'PLC')                      
        #######6th Wave edges creation PLC
        if len(b.edges()) < nb*4:
             sixth_wave = []
             w6_PLC= int(round(np.random.uniform(1000,2000), 0))#1000,2000
             for w in range (w6_PLC): #
                             
                     r= int(round(np.random.uniform(1,8), 0))
                     r_w5_plc = int(round(np.random.uniform(0,w5_PLC-1), 0))
                     test_edges = b.edges()
                     e = (str(fifth_wave[r_w5_plc]), str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+w5_PLC+w+1))
                     f = (str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+w5_PLC+w+1) , str(fifth_wave[r_w5_plc]))
                     while (e in test_edges or f in test_edges or str(fifth_wave[r_w5_plc]) == str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+w5_PLC+w+1)):
                          r= int(round(np.random.uniform(1,8), 0))
                          r_w5_plc = int(round(np.random.uniform(0,w5_PLC-1), 0))
                          e = (str(fifth_wave[r_w5_plc]), str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+w5_PLC+w+1))
                          f = (str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+w5_PLC+w+1) , str(fifth_wave[r_w5_plc]))

                     b.add_edge(fifth_wave[r_w5_plc], str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+w5_PLC+w+1), key = key_rf+w+v+u+t+s+q+p+o+m+l+k+11, color = 'Red', etx=r, mac = 'PLC')
                     sixth_wave.append(str(w1_PLC+w2_PLC+w3_PLC+w4_PLC+w5_PLC+w+1))

             for x in range(len(sixth_wave)/2):
                         r= int(round(np.random.uniform(1,8), 0))
                         test_edges = b.edges()
                         n= int(round(np.random.uniform(1,len(sixth_wave)/2), 0))
                         n2 = int(round(np.random.uniform(1,len(sixth_wave)/2), 0))
                         e = (sixth_wave[n], sixth_wave[n2])
                         f = (sixth_wave[n2], sixth_wave[n])
                            
                         while (e in test_edges or f in test_edges or sixth_wave[n2] == sixth_wave[n]):
                              n= int(round(np.random.uniform(1,len(sixth_wave)/2), 0))
                              n2 = int(round(np.random.uniform(1,len(sixth_wave)/2), 0))
                              
                              e = (sixth_wave[n], sixth_wave[n2])
                              f = (sixth_wave[n2], sixth_wave[n])
                            
                         b.add_edge(sixth_wave[n], sixth_wave[n2], key = key_rf+w+x+u+v+s+t+p+q+m+o+l+k+12, color = 'Red', etx=r, mac = 'PLC')


        #print b.edges()


############## Adapte graph size

        a_nodes = a.nodes()
        b_nodes = b.nodes()

        diff = len(a_nodes) - nb
        diff_2 = len(b_nodes) - nb

       # print diff, diff_2
        if len(a_nodes) > nb:
             for i in range(diff):
                  a.remove_node(str(nb+i))
                         
        if len(b_nodes) > nb:
             for i in range(diff_2):
                  b.remove_node(str(nb+i))                

       # print a.nodes()
 #       print b.nodes()


##################################
        ##################################
        ##################################
        ##################################
##################################

##########################
#Two graphs to multigraph

        list_noeudsA = list(a.edges()) #
        list_noeudsB = list(b.edges()) #
        all_nodesA = a.nodes()
        
        attrib_nodeA = nx.get_node_attributes(a, 'rank')
        #attrib_nodeB = nx.get_node_attributes(b, 'rank')
        for i in range(len(all_nodesA)):# On va chercher tous les rangs du graph original pour ajout
                graph_user.add_node(all_nodesA[i], rank = attrib_nodeA[all_nodesA[i]])
                
        for t in range(len(list_noeudsA)):
                attrib = a.get_edge_data(list_noeudsA[t][0],list_noeudsA[t][1])
                for k in range(len(attrib)):
                    cle = attrib.keys()
                    graph_user.add_edge(list_noeudsA[t][0],list_noeudsA[t][1], key = attrib['key'], color = 'Blue', etx = attrib['etx'], mac = 'RF')
        for t in range(len(list_noeudsB)):
                attrib = b.get_edge_data(list_noeudsB[t][0],list_noeudsB[t][1])
                for k in range(len(attrib)):
                    cle = attrib.keys()
                    graph_user.add_edge(list_noeudsB[t][0],list_noeudsB[t][1], key = attrib['key'], color = 'Red', etx = attrib['etx'], mac = 'PLC')
        print "Nombre de liens graph_user: ", len(graph_user.edges())

print "Hybrid architecture generation..."
#graph_random(node_number)
graph_random_2(node_number)
##
###Test avec un graph fixe
##graph_user.add_node('Root', rank = 256)
##graph_user.add_node('1', rank = 65536)
##graph_user.add_node('2', rank = 65536)
##graph_user.add_node('3', rank = 65536)
##graph_user.add_node('4', rank = 65536)
##graph_user.add_node('5', rank = 65536)
##graph_user.add_node('6', rank = 65536)
##graph_user.add_node('7', rank = 65536)
##graph_user.add_node('8', rank = 65536)
##graph_user.add_node('9', rank = 65536)
##graph_user.add_node('10', rank = 65536)
##graph_user.add_node('11', rank = 65536)
##graph_user.add_node('12', rank = 65536)
##graph_user.add_node('13', rank = 65536)
##graph_user.add_node('14', rank = 65536)
##graph_user.add_node('15', rank = 65536)
##graph_user.add_node('16', rank = 65536)
##graph_user.add_node('17', rank = 65536)
##graph_user.add_node('18', rank = 65536)
##graph_user.add_node('19', rank = 65536)
##graph_user.add_node('20', rank = 65536)
##graph_user.add_node('21', rank = 65536)
##graph_user.add_node('22', rank = 65536)
##graph_user.add_node('23', rank = 65536)
##graph_user.add_node('24', rank = 65536)
##graph_user.add_node('25', rank = 65536)
##graph_user.add_node('26', rank = 65536)
##graph_user.add_node('27', rank = 65536)
##graph_user.add_node('28', rank = 65536)
##graph_user.add_node('29', rank = 65536)
##graph_user.add_node('30', rank = 65536)

##GRAPH CASE 1 (19 edges)
###graph_user.add_edge('Root','1', key = 1, color = 'Blue', etx = 2, mac = 'RF')
##graph_user.add_edge('2','Root', key = 2, color = 'Blue', etx = 7, mac = 'RF')
##graph_user.add_edge('Root','3', key = 3, color = 'Blue', etx = 1, mac = 'RF')
##
##
##graph_user.add_edge('1','2', key = 4, color = 'Blue', etx = 4, mac = 'RF')
##graph_user.add_edge('1','6', key = 5, color = 'Blue', etx = 4, mac = 'RF')
##graph_user.add_edge('1','7', key = 6, color = 'Blue', etx = 7, mac = 'RF')
##graph_user.add_edge('2','4', key = 7, color = 'Blue', etx = 6, mac = 'RF')
##graph_user.add_edge('2','6', key = 8, color = 'Blue', etx = 8, mac = 'RF')
##graph_user.add_edge('5','3', key = 9, color = 'Blue', etx = 4, mac = 'RF')
##graph_user.add_edge('6','7', key = 10, color = 'Blue', etx = 8, mac = 'RF')
##graph_user.add_edge('Root','5', key = 11, color = 'Blue', etx = 7, mac = 'RF')
##graph_user.add_edge('Root','6', key = 12, color = 'Blue', etx = 7, mac = 'RF')
##
##graph_user.add_edge('Root','1', key = 20, color = 'Red', etx = 6, mac = 'PLC')
##graph_user.add_edge('Root','3', key = 21, color = 'Red', etx = 3, mac = 'PLC')
##graph_user.add_edge('Root','7', key = 22, color = 'Red', etx = 5, mac = 'PLC')
##graph_user.add_edge('1','7', key = 23, color = 'Red', etx = 2, mac = 'PLC')
##graph_user.add_edge('3','2', key = 24, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('6','2', key = 25, color = 'Red', etx = 1, mac = 'PLC')
##graph_user.add_edge('3','5', key = 26, color = 'Red', etx = 7, mac = 'PLC')
##graph_user.add_edge('6','4', key = 27, color = 'Red', etx = 8, mac = 'PLC')
##graph_user.add_edge('6','7', key = 28, color = 'Red', etx = 5, mac = 'PLC')
##graph_user.add_edge('2','5', key = 29, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('4','5', key = 30, color = 'Red', etx = 7, mac = 'PLC')

##GRAPH CASE 2 (10 edges)
##graph_user.add_edge('Root','2', key = 1, color = 'Blue', etx = 6, mac = 'RF')
##graph_user.add_edge('2','5', key = 2, color = 'Blue', etx = 3, mac = 'RF')
##graph_user.add_edge('5','7', key = 3, color = 'Blue', etx = 5, mac = 'RF')
###graph_user.add_edge('1','2', key = 4, color = 'Blue', etx = 2, mac = 'RF')
##
##
##graph_user.add_edge('Root','1', key = 20, color = 'Red', etx = 2, mac = 'PLC')
##graph_user.add_edge('1','3', key = 21, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('7','3', key = 22, color = 'Red', etx = 1, mac = 'PLC')
##graph_user.add_edge('4','3', key = 23, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('Root','2', key = 24, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('2','5', key = 25, color = 'Red', etx = 7, mac = 'PLC')
##graph_user.add_edge('5','6', key = 26, color = 'Red', etx = 6, mac = 'PLC')

##GRAPH CASE 3 (10 edges)
##graph_user.add_edge('Root','1', key = 1, color = 'Red', etx = 6, mac = 'PLC')
##graph_user.add_edge('2','Root', key = 2, color = 'Red', etx = 3, mac = 'PLC')
##graph_user.add_edge('1','3', key = 3, color = 'Red', etx = 5, mac = 'PLC')
##graph_user.add_edge('3','2', key = 4, color = 'Red', etx = 2, mac = 'PLC')
##graph_user.add_edge('3','4', key = 5, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('3','5', key = 6, color = 'Red', etx = 9, mac = 'PLC')
##graph_user.add_edge('3','6', key = 7, color = 'Red', etx = 6, mac = 'PLC')
##graph_user.add_edge('5','7', key = 8, color = 'Red', etx = 5, mac = 'PLC')
##
##graph_user.add_edge('Root','1', key = 20, color = 'Blue', etx = 2, mac = 'RF')
##graph_user.add_edge('Root','2', key = 21, color = 'Blue', etx = 4, mac = 'RF')
##graph_user.add_edge('2','3', key = 22, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('4','3', key = 23, color = 'Blue', etx = 4, mac = 'RF')
##graph_user.add_edge('3','5', key = 24, color = 'Blue', etx = 8, mac = 'RF')
###graph_user.add_edge('3','7', key = 25, color = 'Blue', etx = 2, mac = 'RF')

####GRAPH CASE REVIEW (6 edges)
##graph_user.add_edge('Root','1', key = 1, color = 'Red', etx = 1, mac = 'PLC')
##graph_user.add_edge('1','2', key = 2, color = 'Red', etx = 2, mac = 'PLC')
##graph_user.add_edge('Root','3', key = 3, color = 'Red', etx = 3, mac = 'PLC')
##graph_user.add_edge('1','3', key = 4, color = 'Red', etx = 2, mac = 'PLC')
##graph_user.add_edge('2','3', key = 5, color = 'Red', etx = 1, mac = 'PLC')
##
##graph_user.add_edge('Root','1', key = 10, color = 'Blue', etx = 4, mac = 'RF')
##graph_user.add_edge('2','3', key = 11, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('Root','3', key = 12, color = 'Blue', etx = 2, mac = 'RF')
##graph_user.add_edge('1','3', key = 13, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('2','1', key = 14, color = 'Blue', etx = 2, mac = 'RF')


##GRAPH CASE 4 ( edges)
##graph_user.add_edge('Root','1', key = 1, color = 'Red', etx = 8, mac = 'PLC')
##graph_user.add_edge('4','Root', key = 2, color = 'Red', etx = 8, mac = 'PLC')
##graph_user.add_edge('Root','7', key = 3, color = 'Red', etx = 4, mac = 'PLC')
##
##
##graph_user.add_edge('1','2', key = 4, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('2','3', key = 5, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('4','5', key = 6, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('5','6', key = 7, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('7','8', key = 8, color = 'Red', etx = 2, mac = 'PLC')
##graph_user.add_edge('8','9', key = 9, color = 'Red', etx = 2, mac = 'PLC')
##
####
##graph_user.add_edge('1','4', key = 10, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('4','7', key = 11, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('2','5', key = 12, color = 'Blue', etx = 1, mac = 'RF')
##
###### RF Edges for case 4-1: Root-4;1-4;4-7;4-5;2-5;5-8;5-6;3-6;6-9 
##graph_user.add_edge('1','2', key = 13, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('5','6', key = 14, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('6','9', key = 15, color = 'Blue', etx = 1, mac = 'RF')
###graph_user.add_edge('5','4', key = 16, color = 'Blue', etx = 2, mac = 'RF')
###graph_user.add_edge('5','8', key = 17, color = 'Blue', etx = 2, mac = 'RF')
###graph_user.add_edge('3','6', key = 18, color = 'Blue', etx = 2, mac = 'RF')

##GRAPH CASE itron ( edges)
##graph_user.add_edge('Root','1', key = 1, color = 'Red', etx = 8, mac = 'PLC')
##graph_user.add_edge('1','2', key = 2, color = 'Red', etx = 8, mac = 'PLC')
##graph_user.add_edge('2','3', key = 3, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('3','4', key = 4, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('4','5', key = 5, color = 'Red', etx = 4, mac = 'PLC')
###graph_user.add_edge('5','6', key = 6, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('6','7', key = 7, color = 'Red', etx = 4, mac = 'PLC')
###graph_user.add_edge('7','8', key = 8, color = 'Red', etx = 2, mac = 'PLC')
##graph_user.add_edge('8','9', key = 9, color = 'Red', etx = 2, mac = 'PLC')
##graph_user.add_edge('9','10', key = 10, color = 'Red', etx = 4, mac = 'PLC')
###graph_user.add_edge('10','11', key = 11, color = 'Red', etx = 4, mac = 'PLC')
###graph_user.add_edge('11','12', key = 12, color = 'Red', etx = 8, mac = 'PLC')
##graph_user.add_edge('12','13', key = 13, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('13','14', key = 14, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('14','15', key = 15, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('15','16', key = 16, color = 'Red', etx = 4, mac = 'PLC')
###graph_user.add_edge('16','17', key = 17, color = 'Red', etx = 2, mac = 'PLC')
###graph_user.add_edge('17','18', key = 18, color = 'Red', etx = 2, mac = 'PLC')
###graph_user.add_edge('18','19', key = 19, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('19','20', key = 20, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('20','21', key = 21, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('21','22', key = 22, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('22','23', key = 23, color = 'Red', etx = 8, mac = 'PLC')
##graph_user.add_edge('23','24', key = 24, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('24','25', key = 25, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('25','26', key = 26, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('26','27', key = 27, color = 'Red', etx = 4, mac = 'PLC')
##graph_user.add_edge('27','28', key = 28, color = 'Red', etx = 2, mac = 'PLC')
##graph_user.add_edge('28','29', key = 29, color = 'Red', etx = 2, mac = 'PLC')
##graph_user.add_edge('29','30', key = 30, color = 'Red', etx = 4, mac = 'PLC')
##
##
####
##graph_user.add_edge('Root','1', key = 101, color = 'Blue', etx = 2, mac = 'RF')
##graph_user.add_edge('1','2', key = 102, color = 'Blue', etx = 5, mac = 'RF')
##graph_user.add_edge('2','3', key = 103, color = 'Blue', etx = 5, mac = 'RF')
##graph_user.add_edge('3','4', key = 104, color = 'Blue', etx = 8, mac = 'RF')
##graph_user.add_edge('4','5', key = 105, color = 'Blue', etx = 4, mac = 'RF')
##graph_user.add_edge('5','6', key = 106, color = 'Blue', etx = 3, mac = 'RF')
###graph_user.add_edge('6','7', key = 107, color = 'Blue', etx = 2, mac = 'RF')
##graph_user.add_edge('7','8', key = 108, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('8','9', key = 109, color = 'Blue', etx = 3, mac = 'RF')
##graph_user.add_edge('9','10', key = 110, color = 'Blue', etx = 8, mac = 'RF')
##graph_user.add_edge('10','11', key = 111, color = 'Blue', etx = 2, mac = 'RF')
##graph_user.add_edge('11','12', key = 112, color = 'Blue', etx = 6, mac = 'RF')
###graph_user.add_edge('12','13', key = 113, color = 'Blue', etx = 5, mac = 'RF')
###graph_user.add_edge('13','14', key = 114, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('14','15', key = 115, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('15','16', key = 116, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('16','17', key = 117, color = 'Blue', etx = 6, mac = 'RF')
##graph_user.add_edge('17','18', key = 118, color = 'Blue', etx = 3, mac = 'RF')
##graph_user.add_edge('18','19', key = 119, color = 'Blue', etx = 3, mac = 'RF')
##graph_user.add_edge('19','20', key = 120, color = 'Blue', etx = 5, mac = 'RF')
###graph_user.add_edge('20','21', key = 120, color = 'Blue', etx = 5, mac = 'RF')
##graph_user.add_edge('21','22', key = 121, color = 'Blue', etx = 2, mac = 'RF')
##graph_user.add_edge('22','23', key = 122, color = 'Blue', etx = 4, mac = 'RF')
##graph_user.add_edge('23','24', key = 123, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('24','25', key = 124, color = 'Blue', etx = 1, mac = 'RF')
###graph_user.add_edge('25','26', key = 125, color = 'Blue', etx = 5, mac = 'RF')
###graph_user.add_edge('26','27', key = 126, color = 'Blue', etx = 4, mac = 'RF')
###graph_user.add_edge('27','28', key = 127, color = 'Blue', etx = 3, mac = 'RF')
##graph_user.add_edge('28','29', key = 128, color = 'Blue', etx = 1, mac = 'RF')
##graph_user.add_edge('29','30', key = 129, color = 'Blue', etx = 3, mac = 'RF')




def nb_child(nb, nbr, Graph, AGraph):
        attrib_node = nx.get_node_attributes(AGraph, 'rank')
        
        nbd = []
        for o in range(len(nb)):#Looking for neighbors
                
                nb2 =  nx.neighbors(Graph, nb[o])
                for i in range(len(nbr)):
                        if nbr[i] in nb2:
                                nb2.remove(nbr[i]) # Delete parents if present
                
                for i in range(len(nb)):
                        if nb[i] in nb2:
                                nb2.remove(nb[i]) # delete known nodes if presents





                nbd.extend(nb2)                               
                for i in range(len(nb2)): #on ajoute les liens du root vers ses voisins
                        for j in range(len(nb)):
                                attrib = Graph.get_edge_data(nb2[i],nb[j], default=0)
                                

                                
                                if attrib != 0:
                                        cle = attrib.keys()
                                        attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        if attrib_node[nb2[i]] > attrib_node[nb[j]]:
  
                                                attrib_inv = AGraph.get_edge_data(nb[j], nb2[i], default=0)
                                                if attrib_inv != 0:
                                                        AGraph.remove_edge(nb[j],nb2[i])
                                                AGraph.add_edge(nb2[i],nb[j], color = attrib['color'], key = attrib['key'], etx = attrib['etx'], mac = attrib['mac'])
                                        elif attrib_node[nb2[i]] < attrib_node[nb[j]]:
 
                                                attrib_inv = AGraph.get_edge_data(nb2[i], nb[j], default=0)
                                                if attrib_inv != 0:
                                                        AGraph.remove_edge(nb2[i], nb[j])
                                                AGraph.add_edge(nb[j], nb2[i], color = attrib['color'], key = attrib['key'], etx = attrib['etx'], mac = attrib['mac'])
                                                        #il faut maintenant calculer les rangs des voisins du root, fonction du type R(Root)+etx*minHop
                                        attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        rank = round(attrib_node[nb[j]]+attrib['etx']*256)
                                        if rank < attrib_node[nb2[i]]:
                                                AGraph.add_node(nb2[i], rank = round(attrib_node[nb[j]]+attrib['etx']*256))
                                                #print "ajout noeud2 ", nb2[i], namestr(AGraph, globals()), round(attrib_node[nb[j]]+attrib['etx']*256)

                                        ###test successors
                                        success = AGraph.successors(nb2[i])
                                        for k in range(len(success)):
                                                if attrib_node[success[k]] > rank:
                                                        attrib_temp = AGraph.get_edge_data(nb2[i], success[k], default=0)
                                                        AGraph.remove_edge(nb2[i], success[k])
                                                        AGraph.add_edge(success[k], nb2[i], color = attrib_temp['color'], key = attrib['key'], etx = attrib_temp['etx'], mac = attrib_temp['mac'])
                                        ####fin test successors
                                        predecess = AGraph.predecessors(nb2[i])
                                        attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        
                                        for l in range(len(predecess)):
                                             attrib_temp = AGraph.get_edge_data( predecess[l], nb2[i], default=0)
                                             rank = attrib_temp['etx']*256+attrib_node[nb2[i]]
                                             if attrib_node[predecess[l]] > rank:
                                                  AGraph.add_node(predecess[l], rank = attrib_temp['etx']*256+attrib_node[nb2[i]])
                                        #else:
                                                #print rank, attrib_node[nb2[i]]
                                #else:
                                        #print "Pas de lien"
        nbd = list(set(nbd))
        return nbd

def nb_link(nb, Graph, AGraph):
        i = len(set(nb))
        i -= 1
        nbd = list(set(nb))
        for k in range (i):
            for j in range(i-k):

                attrib = Graph.get_edge_data(nb[j],nb[j+k+1], default=0)
                
                if attrib != 0:
                        attrib_node = nx.get_node_attributes(AGraph, 'rank')
                        if attrib_node[nb[j]] > attrib_node[nb[j+k+1]]:
                                attrib_inv = AGraph.get_edge_data(nb[j+k+1], nb[j], default=0)
                                if attrib_inv != 0:
                                        AGraph.remove_edge(nb[j+k+1],nb[j])
                                AGraph.add_edge(nb[j],nb[j+k+1], color = attrib['color'], key = attrib['key'], etx = attrib['etx'], mac = attrib['mac'])
                                rank = round(attrib_node[nb[j+k+1]]+attrib['etx']*256)
                                if rank < attrib_node[nb[j]]:
                                        AGraph.add_node(nb[j], rank = round(attrib_node[nb[j+k+1]]+attrib['etx']*256))
                                        #print "ajout noeud3 ", nb[j], namestr(AGraph, globals()), round(attrib_node[nb[j+k+1]]+attrib['etx']*256)
                                ###test successors
                                success = AGraph.successors(nb[j])
                                for p in range(len(success)):
                                        if attrib_node[success[p]] > rank:
                                                attrib_temp = AGraph.get_edge_data(nb[j], success[p], default=0)
                                                AGraph.remove_edge(nb[j], success[p])
                                                AGraph.add_edge(success[p], nb[j], color = attrib_temp['color'], key = attrib['key'], etx = attrib_temp['etx'], mac = attrib_temp['mac'])
                                ####fin test successors



                                predecess = AGraph.predecessors(nb[j])
                                attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        
                                for l in range(len(predecess)):
                                     attrib_temp = AGraph.get_edge_data( predecess[l], nb[j], default=0)
                                     rank = attrib_temp['etx']*256+attrib_node[nb[j]]
                                     if attrib_node[predecess[l]] > rank:
                                          AGraph.add_node(predecess[l], rank = attrib_temp['etx']*256+attrib_node[nb[j]])
                                
                        elif attrib_node[nb[j]] < attrib_node[nb[j+k+1]]:
                                attrib_inv = AGraph.get_edge_data( nb[j], nb[j+k+1], default=0)
                                if attrib_inv != 0:
                                        AGraph.remove_edge(nb[j], nb[j+k+1],)
                                AGraph.add_edge(nb[j+k+1], nb[j], color = attrib['color'], key = attrib['key'], etx = attrib['etx'], mac = attrib['mac'])
                                rank = round(attrib_node[nb[j]]+attrib['etx']*256)
                                if rank < attrib_node[nb[j+k+1]]:
                                        AGraph.add_node(nb[j+k+1], rank = round(attrib_node[nb[j]]+attrib['etx']*256))
                                        #print "ajout noeud4 ", nb[j+k+1], namestr(AGraph, globals()), round(attrib_node[nb[j]]+attrib['etx']*256)

                                ###test successors
                                success = AGraph.successors(nb[j+k+1])
                                for p in range(len(success)):
                                        if attrib_node[success[p]] > rank:
                                                attrib_temp = AGraph.get_edge_data(nb[j+k+1], success[p], default=0)
                                                AGraph.remove_edge(nb[j+k+1], success[p])
                                                AGraph.add_edge(success[p], nb[j+k+1], color = attrib_temp['color'], key = attrib['key'], etx = attrib_temp['etx'], mac = attrib_temp['mac'])
                                ####fin test successors


                                predecess = AGraph.predecessors(nb[j+k+1])
                                attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        
                                for l in range(len(predecess)):
                                     attrib_temp = AGraph.get_edge_data( predecess[l], nb[j+k+1], default=0)
                                     rank = attrib_temp['etx']*256+attrib_node[nb[j+k+1]]
                                     if attrib_node[predecess[l]] > rank:
                                          AGraph.add_node(predecess[l], rank = attrib_temp['etx']*256+attrib_node[nb[j+k+1]])

                                          
                        #elif attrib_node[nb[j]] == attrib_node[nb[j+k+1]]:
                                #print "Identical rank"
                #else:
                        #print 'Pas de lien'



############################################################
###              DAG CREATION Interface Oriented version



def nb_child2(nb, nbr, Graph, AGraph):
        attrib_node = nx.get_node_attributes(AGraph, 'rank')
        
        nbd = []
        for o in range(len(nb)):#Looking for neighbors
                
                nb2 =  nx.neighbors(Graph, nb[o])
                for i in range(len(nbr)):
                        if nbr[i] in nb2:
                                nb2.remove(nbr[i]) # Delete parents if present
                
                for i in range(len(nb)):
                        if nb[i] in nb2:
                                nb2.remove(nb[i]) # delete known nodes if presents





                nbd.extend(nb2)                               
                for i in range(len(nb2)): #on ajoute les liens du root vers ses voisins
                        for j in range(len(nb)):
                                attrib = Graph.get_edge_data(nb2[i],nb[j], default=0)

                                
                                if attrib != 0:
                                     cle = attrib.keys()
                                     if len(attrib) == 1 :
                                          
                                          
                                          cle = attrib.keys()
                                          
                                          attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                          if attrib_node[nb2[i]] > attrib_node[nb[j]]:
                                                  attrib_inv = AGraph.get_edge_data(nb[j], nb2[i], default=0)
                                                  if attrib_inv != 0:
                                                          AGraph.remove_edge(nb[j],nb2[i])
                                                  AGraph.add_edge(nb2[i],nb[j], color = attrib[cle[0]]['color'], key = cle[0], etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'])
                                                      
                                                                

                                          elif attrib_node[nb2[i]] < attrib_node[nb[j]]:
                                                  attrib_inv = AGraph.get_edge_data(nb2[i], nb[j], default=0)
                                                  if attrib_inv != 0:
                                                          AGraph.remove_edge(nb2[i], nb[j])
                                                  AGraph.add_edge(nb[j], nb2[i], color = attrib[cle[0]]['color'], key = cle[0], etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'])
 
                                          attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                          rank = round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256)
        
                                          if rank < attrib_node[nb2[i]]:
                                                  AGraph.add_node(nb2[i], rank = round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256))

                                          success = AGraph.successors(nb2[i])
                                        
                                          attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                          for k in range(len(success)):
                                                  if attrib_node[success[k]] > rank:
                                                          attrib_temp = AGraph.get_edge_data(nb2[i], success[k], default=0)
                                                          AGraph.remove_edge(nb2[i], success[k])
                                                          AGraph.add_edge(success[k], nb2[i], color = attrib_temp['color'], key = attrib_temp['key'], etx = attrib_temp['etx'], mac = attrib_temp['mac'])

                                          predecess = AGraph.predecessors(nb2[i])
                                          attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        
                                          for l in range(len(predecess)):
                                               attrib_temp = AGraph.get_edge_data( predecess[l], nb2[i], default=0)
                                               rank = attrib_temp['etx']*256+attrib_node[nb2[i]]
                                               if attrib_node[predecess[l]] > rank:
                                                    AGraph.add_node(predecess[l], rank = attrib_temp['etx']*256+attrib_node[nb2[i]])




                                     if len(attrib) == 2 :
                                          ######sera à revoir pour ajouter les policies

                                          if attrib[cle[0]]['etx'] > attrib[cle[1]]['etx']:
                                               attrib.pop(cle[0], None)
                                          else:
                                               attrib.pop(cle[1], None)
                                                                                    
                                          cle = attrib.keys()
                                          attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                          if attrib_node[nb2[i]] > attrib_node[nb[j]]:
                                                  attrib_inv = AGraph.get_edge_data(nb[j], nb2[i], default=0)
                                                  if attrib_inv != 0:
                                                          AGraph.remove_edge(nb[j],nb2[i])
                                                  AGraph.add_edge(nb2[i],nb[j], color = attrib[cle[0]]['color'], key = cle[0], etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'])
                                                      
                                                                

                                          elif attrib_node[nb2[i]] < attrib_node[nb[j]]:
                                                  attrib_inv = AGraph.get_edge_data(nb2[i], nb[j], default=0)
                                                  if attrib_inv != 0:
                                                          AGraph.remove_edge(nb2[i], nb[j])
                                                  AGraph.add_edge(nb[j], nb2[i], color = attrib[cle[0]]['color'], key = cle[0], etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'])
 
                                          attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                          rank = round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256)
        
                                          if rank < attrib_node[nb2[i]]:
                                                  AGraph.add_node(nb2[i], rank = round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256))

                                          success = AGraph.successors(nb2[i])
                                          attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                          for k in range(len(success)):
                                                  if attrib_node[success[k]] > rank:
                                                          attrib_temp = AGraph.get_edge_data(nb2[i], success[k], default=0)
                                                          AGraph.remove_edge(nb2[i], success[k])
                                                          AGraph.add_edge(success[k], nb2[i], color = attrib_temp['color'], key = attrib_temp['key'], etx = attrib_temp['etx'], mac = attrib_temp['mac'])

                                          predecess = AGraph.predecessors(nb2[i])
                                          attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        
                                          for l in range(len(predecess)):
                                               attrib_temp = AGraph.get_edge_data( predecess[l], nb2[i], default=0)
                                               rank = attrib_temp['etx']*256+attrib_node[nb2[i]]
                                               if attrib_node[predecess[l]] > rank:
                                                    AGraph.add_node(predecess[l], rank = attrib_temp['etx']*256+attrib_node[nb2[i]])
                                        
        nbd = list(set(nbd))
        return nbd

def nb_link2(nb, Graph, AGraph):
        i = len(set(nb))
        i -= 1
        nbd = list(set(nb))
        for k in range (i):
            for j in range(i-k):

                attrib = Graph.get_edge_data(nb[j],nb[j+k+1], default=0)

                        
                        
                if attrib != 0:
                        cle = attrib.keys()
                        if len(attrib) == 1 :
                                attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                if attrib_node[nb[j]] > attrib_node[nb[j+k+1]]:

                                        AGraph.add_edge(nb[j],nb[j+k+1], color = attrib[cle[0]]['color'], key = cle[0], etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'])
                                        rank = round(attrib_node[nb[j+k+1]]+attrib[cle[0]]['etx']*256)
                                        #print rank, nb[j], attrib_node[nb[j]]
                                        if rank < attrib_node[nb[j]]:
                                                AGraph.add_node(nb[j], rank = round(attrib_node[nb[j+k+1]]+attrib[cle[0]]['etx']*256))
                                                #print "B" ,nb[j], round(attrib_node[nb[j+k+1]]+attrib[cle[0]]['etx']*256)
                                        ###test successors
                                        success = AGraph.successors(nb[j])
                                        #print "2", nb[j], success
                                        for p in range(len(success)):
                                                if attrib_node[success[p]] > rank:
                                                        attrib_temp = AGraph.get_edge_data(nb[j], success[p], default=0)
                                                        AGraph.remove_edge(nb[j], success[p])
                                                        AGraph.add_edge(success[p], nb[j], color = attrib_temp['color'], key = attrib_temp['key'], etx = attrib_temp['etx'], mac = attrib[cle[0]]['mac'])
                                        #######################
                                        predecess = AGraph.predecessors(nb[j])
                                        attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        
                                        for l in range(len(predecess)):
                                             attrib_temp = AGraph.get_edge_data( predecess[l], nb[j], default=0)
                                             rank = attrib_temp['etx']*256+attrib_node[nb[j]]
                                             if attrib_node[predecess[l]] > rank:
                                                  AGraph.add_node(predecess[l], rank = attrib_temp['etx']*256+attrib_node[nb[j]])
                                        ############################
                                        ####fin test successors


                                elif attrib_node[nb[j]] < attrib_node[nb[j+k+1]]:

                                        AGraph.add_edge(nb[j+k+1], nb[j], color = attrib[cle[0]]['color'], key = cle[0], etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'])
                                        rank = round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256)
                                        #print rank, nb[j+k+1], attrib_node[nb[j+k+1]]
                                        if rank < attrib_node[nb[j+k+1]]:
                                                AGraph.add_node(nb[j+k+1], rank = round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256))
                                                #print "C" ,nb[j+k+1], round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256)
                                        ###test successors
                                        success = AGraph.successors(nb[j+k+1])
                                        #print "3", nb[j+k+1], success
                                        for p in range(len(success)):
                                                if attrib_node[success[p]] > rank:
                                                        attrib_temp = AGraph.get_edge_data(nb[j+k+1], success[p], default=0)
                                                        AGraph.remove_edge(nb[j+k+1], success[p])
                                                        AGraph.add_edge(success[p], nb[j+k+1], color = attrib_temp['color'], key = attrib_temp['key'], etx = attrib_temp['etx'], mac = attrib_temp['mac'])

                                        #######################
                                        predecess = AGraph.predecessors(nb[j+k+1])
                                        attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        
                                        for l in range(len(predecess)):
                                             attrib_temp = AGraph.get_edge_data( predecess[l], nb[j+k+1], default=0)
                                             rank = attrib_temp['etx']*256+attrib_node[nb[j+k+1]]
                                             if attrib_node[predecess[l]] > rank:
                                                  AGraph.add_node(predecess[l], rank = attrib_temp['etx']*256+attrib_node[nb[j+k+1]])
                                        ############################
                                        ####fin test successors



                                        
                        if len(attrib) == 2 :
                                #print attrib
                                if attrib[cle[0]]['etx'] > attrib[cle[1]]['etx']:
                                        attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        if attrib_node[nb[j]] > attrib_node[nb[j+k+1]]:

                                                AGraph.add_edge(nb[j],nb[j+k+1], color = attrib[cle[1]]['color'], key = cle[1], etx = attrib[cle[1]]['etx'], mac = attrib[cle[1]]['mac'])
                                                
                                                rank = round(attrib_node[nb[j+k+1]]+attrib[cle[0]]['etx']*256)
                                                #print rank, nb[j], attrib_node[nb[j]]
                                                if rank < attrib_node[nb[j]]:
                                                        AGraph.add_node(nb[j], rank = round(attrib_node[nb[j+k+1]]+attrib[cle[0]]['etx']*256))
                                                        #print "D" ,nb[j], round(attrib_node[nb[j+k+1]]+attrib[cle[0]]['etx']*256)
                                                ###test successors
                                                success = AGraph.successors(nb[j])
                                                #print  "4", nb[j], success
                                                for p in range(len(success)):
                                                        if attrib_node[success[p]] > rank:
                                                                attrib_temp = AGraph.get_edge_data(nb[j], success[p], default=0)
                                                                AGraph.remove_edge(nb[j], success[p])
                                                                AGraph.add_edge(success[p], nb[j], color = attrib_temp['color'], key = attrib_temp['key'], etx = attrib_temp['etx'], mac = attrib_temp['mac'])
                                                #######################
                                                predecess = AGraph.predecessors(nb[j])
                                                attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        
                                                for l in range(len(predecess)):
                                                     attrib_temp = AGraph.get_edge_data( predecess[l], nb[j], default=0)
                                                     rank = attrib_temp['etx']*256+attrib_node[nb[j]]
                                                     if attrib_node[predecess[l]] > rank:
                                                          AGraph.add_node(predecess[l], rank = attrib_temp['etx']*256+attrib_node[nb[j]])
                                        ############################
                                                ####fin test successors

                                                
                                        elif attrib_node[nb[j]] < attrib_node[nb[j+k+1]]:

                                                AGraph.add_edge(nb[j+k+1], nb[j], color = attrib[cle[1]]['color'], key = cle[1], etx = attrib[cle[1]]['etx'], mac = attrib[cle[1]]['mac'])

                                                rank = round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256)
                                                #print rank, nb[j+k+1], attrib_node[nb[j+k+1]]
                                                if rank < attrib_node[nb[j+k+1]]:
                                                        AGraph.add_node(nb[j+k+1], rank = round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256))
                                                        #print "E" ,nb[j+k+1], round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256)
                                                ###test successors
                                                success = AGraph.successors(nb[j+k+1])
                                                #print "5", nb[j+k+1], success
                                                for p in range(len(success)):
                                                        if attrib_node[success[p]] > rank:
                                                                attrib_temp = AGraph.get_edge_data(nb[j+k+1], success[p], default=0)
                                                                AGraph.remove_edge(nb[j+k+1], success[p])
                                                                AGraph.add_edge(success[p], nb[j+k+1], color = attrib_temp['color'], key = attrib_temp['key'], etx = attrib_temp['etx'], mac = attrib_temp['mac'])

                                                #######################
                                                predecess = AGraph.predecessors(nb[j+k+1])
                                                attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        
                                                for l in range(len(predecess)):
                                                     attrib_temp = AGraph.get_edge_data( predecess[l], nb[j+k+1], default=0)
                                                     rank = attrib_temp['etx']*256+attrib_node[nb[j+k+1]]
                                                     if attrib_node[predecess[l]] > rank:
                                                          AGraph.add_node(predecess[l], rank = attrib_temp['etx']*256+attrib_node[nb[j+k+1]])
                                        ############################
                                                ####fin test successors
                                       
                                else:
                                        
             
                                        attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        if attrib_node[nb[j]] > attrib_node[nb[j+k+1]]:
                                                  
                                                AGraph.add_edge(nb[j],nb[j+k+1], color = attrib[cle[0]]['color'], key = cle[0], etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'])

                                                rank = round(attrib_node[nb[j+k+1]]+attrib[cle[0]]['etx']*256)
                                                #print rank, nb[j], attrib_node[nb[j]]
                                                if rank < attrib_node[nb[j]]:
                                                        AGraph.add_node(nb[j], rank = round(attrib_node[nb[j+k+1]]+attrib[cle[0]]['etx']*256))
                                                        #print "F" ,nb[j], round(attrib_node[nb[j+k+1]]+attrib[cle[0]]['etx']*256)
                                                ###test successors
                                                success = AGraph.successors(nb[j])
                                                #print "6", nb[j], success
                                                for p in range(len(success)):
                                                        if attrib_node[success[p]] > rank:
                                                                attrib_temp = AGraph.get_edge_data(nb[j], success[p], default=0)
                                                                AGraph.remove_edge(nb[j], success[p])
                                                                AGraph.add_edge(success[p], nb[j], color = attrib_temp['color'], key = attrib_temp['key'], etx = attrib_temp['etx'], mac = attrib_temp['mac'])
                                                #######################
                                                predecess = AGraph.predecessors(nb[j])
                                                attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        
                                                for l in range(len(predecess)):
                                                     attrib_temp = AGraph.get_edge_data( predecess[l], nb[j], default=0)
                                                     rank = attrib_temp['etx']*256+attrib_node[nb[j]]
                                                     if attrib_node[predecess[l]] > rank:
                                                          AGraph.add_node(predecess[l], rank = attrib_temp['etx']*256+attrib_node[nb[j]])
                                        ############################
                                                ####fin test successors

                                                        
                                        elif attrib_node[nb[j]] < attrib_node[nb[j+k+1]]:

                                                AGraph.add_edge(nb[j+k+1], nb[j], color = attrib[cle[0]]['color'], key = cle[0], etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'])
                                                rank = round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256)
                                                #print rank, nb[j+k+1], attrib_node[nb[j+k+1]]
                                                if rank < attrib_node[nb[j+k+1]]:
                                                        AGraph.add_node(nb[j+k+1], rank = round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256))
                                                        #print "G" ,nb[j+k+1], round(attrib_node[nb[j]]+attrib[cle[0]]['etx']*256)
                                                ###test successors
                                                success = AGraph.successors(nb[j+k+1])
                                                #print "7", nb[j+k+1], success
                                                for p in range(len(success)):
                                                        if attrib_node[success[p]] > rank:
                                                                attrib_temp = AGraph.get_edge_data(nb[j+k+1], success[p], default=0)
                                                                AGraph.remove_edge(nb[j+k+1], success[p])
                                                                AGraph.add_edge(success[p], nb[j+k+1], color = attrib_temp['color'], key = attrib_temp['key'], etx = attrib_temp['etx'], mac = attrib_temp['mac'])
                                                #######################
                                                predecess = AGraph.predecessors(nb[j+k+1])
                                                attrib_node = nx.get_node_attributes(AGraph, 'rank')
                                        
                                                for l in range(len(predecess)):
                                                     attrib_temp = AGraph.get_edge_data( predecess[l], nb[j+k+1], default=0)
                                                     rank = attrib_temp['etx']*256+attrib_node[nb[j+k+1]]
                                                     if attrib_node[predecess[l]] > rank:
                                                          AGraph.add_node(predecess[l], rank = attrib_temp['etx']*256+attrib_node[nb[j+k+1]])
                                        ############################
                                                ####fin test successors
                        
                        #elif attrib_node[nb[j]] == attrib_node[nb[j+k+1]]:
                                #print "Identical rank"
                #else:
                        #print 'Pas de lien'
                                







#Construction d'un DAG à partir d'un graph
###########################################

def DAGcreation(Graph, AGraph):

        nb = []
        nb.append(['Root'])
        nb.append(nx.neighbors(Graph, 'Root'))# voisins du root
        all_nodes = Graph.nodes()
        #attrib_node = nx.get_node_attributes(G, 'rank')############################################ERREUR ref sur G????
        attrib_node = nx.get_node_attributes(Graph, 'rank')
        for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
                #print attrib_node
                AGraph.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])

        for i in range(len(nb[1])): #on ajoute les liens du root vers ses voisins
                
                attrib = Graph.get_edge_data(nb[1][i],'Root', default=0)
                #print attrib
                cle = attrib.keys()
                AGraph.add_edge(nb[1][i],'Root', color = attrib['color'], key = attrib['key'], etx = attrib['etx'], mac = attrib['mac'])#, etx = attrib[cle[k]]['etx'])
                #il faut maintenant calculer les rangs des voisins du root, fonction du type R(Root)+etx*minHop
                rank_test = nx.get_node_attributes(AGraph, 'rank')  ###new
                if (rank_test[nb[1][i]] > round(attrib_node['Root']+attrib['etx']*256)):  #new
                     AGraph.add_node(nb[1][i], rank = round(attrib_node['Root']+attrib['etx']*256))
                     #print "ajout noeud ", nb[1][i], namestr(AGraph, globals()), round(attrib_node['Root']+attrib['etx']*256)
                         
        nb_link(nb[1], Graph, AGraph)
        i = 1
        while nb[i]:

                nb.append(nb_child(nb[i], nb[i-1], Graph, AGraph))
                nb_link(nb[i+1], Graph, AGraph)
                i +=1
        
        #print nx.get_node_attributes(AGraph, 'rank')



#Construction d'un DAG à partir d'un graph
###########################################

def DAGcreation2(Graph, AGraph):

        nb = []
        nb.append(['Root'])
        nb.append(nx.neighbors(Graph, 'Root'))# voisins du root
        all_nodes = Graph.nodes()
        #attrib_node = nx.get_node_attributes(G, 'rank')############################################ERREUR ref sur G????
        attrib_node = nx.get_node_attributes(Graph, 'rank')
        for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
                #print attrib_node
                AGraph.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])

        for i in range(len(nb[1])): #on ajoute les liens du root vers ses voisins
                
                attrib = Graph.get_edge_data(nb[1][i],'Root', default=0)
                cle = attrib.keys()
                #print attrib
                if len(attrib) == 2:
                        if attrib[cle[0]]['etx'] < attrib[cle[1]]['etx']:
                                #print nb[1][i]
                                #print attrib[cle[0]]['etx'] ,attrib[cle[1]]['etx']
                                #print "ajout edge A: ", nb[1][i], 'Root',attrib[cle[0]]['color'],  attrib[cle[0]]['etx'] 
                                AGraph.add_edge(nb[1][i],'Root', color = attrib[cle[0]]['color'], key = cle[0], etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'])#, etx = attrib[cle[k]]['etx'])
                                rank_test = nx.get_node_attributes(AGraph, 'rank')  ###new
                                if (rank_test[nb[1][i]] > round(attrib_node['Root']+attrib[cle[0]]['etx']*256)):  #new
                                        AGraph.add_node(nb[1][i], rank = round(attrib_node['Root']+attrib[cle[0]]['etx']*256))
                        else:
                                #print nb[1][i]
                                #print attrib[cle[0]]['etx'] ,attrib[cle[1]]['etx']
                                #print "ajout edge B: ", nb[1][i], 'Root',attrib[cle[1]]['color'],  attrib[cle[1]]['etx']
                                AGraph.add_edge(nb[1][i],'Root', color = attrib[cle[1]]['color'], key = cle[1], etx = attrib[cle[1]]['etx'], mac = attrib[cle[1]]['mac'])#, etx = attrib[cle[k]]['etx'])
                                rank_test = nx.get_node_attributes(AGraph, 'rank')  ###new
                                if (rank_test[nb[1][i]] > round(attrib_node['Root']+attrib[cle[1]]['etx']*256)):  #new
                                        AGraph.add_node(nb[1][i], rank = round(attrib_node['Root']+attrib[cle[1]]['etx']*256))
                else:
                        AGraph.add_edge(nb[1][i],'Root', color = attrib[cle[0]]['color'], key = cle[0], etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'])#, etx = attrib[cle[k]]['etx'])
                        #print "Ajout edge", nb[1][i], 'Root', attrib[cle[0]]['color'], attrib[cle[0]]['etx']
                #il faut maintenant calculer les rangs des voisins du root, fonction du type R(Root)+etx*minHop
                        rank_test = nx.get_node_attributes(AGraph, 'rank')  ###new
                        if (rank_test[nb[1][i]] > round(attrib_node['Root']+attrib[cle[0]]['etx']*256)):  #new
                                AGraph.add_node(nb[1][i], rank = round(attrib_node['Root']+attrib[cle[0]]['etx']*256))
                #print nb[1][i], round(attrib_node['Root']+attrib[cle[0]]['etx']*256)
       

        #print "VOisins du Root: " , nb[1]
        nb_link2(nb[1], Graph, AGraph)
        i = 1
        while nb[i]:
                #print "Voisins vague", i, ": " , nb[i]
                nb.append(nb_child2(nb[i], nb[i-1], Graph, AGraph))
                nb_link2(nb[i+1], Graph, AGraph)
                i +=1
        
        #print "hello", nx.get_node_attributes(AGraph, 'rank')




#Construction d'un DAG à partir d'un graph PO
###########################################

def DAGcreationPO(Graph, AGraph):

        nb = []
        nb.append(['Root'])
        nb.append(nx.neighbors(Graph, 'Root'))# voisins du root
        all_nodes = Graph.nodes()
        #attrib_node = nx.get_node_attributes(G, 'rank')############################################ERREUR ref sur G????
        attrib_node = nx.get_node_attributes(Graph, 'rank')
        for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
                #print attrib_node
                AGraph.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])

        for i in range(len(nb[1])): #on ajoute les liens du root vers ses voisins
                
                attrib = Graph.get_edge_data(nb[1][i],'Root', default=0)
                #print attrib
                cle = attrib.keys()
                AGraph.add_edge(nb[1][i],'Root', color = attrib['color'], key = attrib['key'], etx = attrib['etx'], mac = attrib['mac'])#, etx = attrib[cle[k]]['etx'])
                #il faut maintenant calculer les rangs des voisins du root, fonction du type R(Root)+etx*minHop
                rank_test = nx.get_node_attributes(AGraph, 'rank')  ###new
                if (rank_test[nb[1][i]] > round(attrib_node['Root']+attrib['etx']*256)):  #new
                        AGraph.add_node(nb[1][i], rank = round(attrib_node['Root']+attrib['etx']*256))
                

        nb_link(nb[1], Graph, AGraph)
        i = 1
        while nb[i]:

                nb.append(nb_child(nb[i], nb[i-1], Graph, AGraph))
                nb_link(nb[i+1], Graph, AGraph)
                i +=1
        
        #print nx.get_node_attributes(AGraph, 'rank')




#Construction de deux DAG
###########################
def two_dags(G, DAGPLC, DAGRF):
        list_noeuds = list(set(G.edges())) #on vire les doublons au passage
        
        all_nodes = G.nodes()
        
        attrib_node = nx.get_node_attributes(G, 'rank')
        
        for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
                DAGPLC.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])
                DAGRF.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])
        for t in range(len(list_noeuds)):
                attrib = G.get_edge_data(list_noeuds[t][0],list_noeuds[t][1])
                for k in range(len(attrib)):
                    cle = attrib.keys()
                    if attrib[cle[k]]['mac'] == 'PLC':
                        DAGPLC.add_edge(list_noeuds[t][0],list_noeuds[t][1], key = cle[k], color = 'Red', etx = attrib[cle[k]]['etx'], mac = attrib[cle[k]]['mac'])
                    else:
                        DAGRF.add_edge(list_noeuds[t][0],list_noeuds[t][1], key = cle[k], color = 'Blue', etx = attrib[cle[k]]['etx'], mac = attrib[cle[k]]['mac'])


#Two DAGs to Multi Di graph 1 = RF, 2= PLC
#######################################
def dags_to_multigraph(DAG1, DAG2, MultiGraph):



        nb = DAG1.edges()

        all_nodes = DAG1.nodes()
        all_nodes2 = DAG2.nodes()
        attrib_node = nx.get_node_attributes(DAG1, 'rank')
        attrib_node2 = nx.get_node_attributes(DAG2, 'rank')
        for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
                #print all_nodes
                MultiGraph.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]], rank_plc = attrib_node2[all_nodes2[i]])

        for i in range(len(nb)): #on ajoute les liens du root vers ses voisins
                attrib = DAG1.get_edge_data(nb[i][0], nb[i][1], default=0)
                
                MultiGraph.add_edge(nb[i][0], nb[i][1], key = attrib['key'], color = attrib['color'], etx = attrib['etx'], mac = attrib['mac'])#, etx = attrib[cle[k]]['etx'])
        nb = DAG2.edges()
        for j in range(len(nb)): #on ajoute les liens du root vers ses voisins
                
                attrib = DAG2.get_edge_data(nb[j][0], nb[j][1], default=0)
                
                MultiGraph.add_edge(nb[j][0], nb[j][1], key = attrib['key'], color = attrib['color'], etx = attrib['etx'], mac = attrib['mac'])#, etx = attrib[cle[k]]['etx'])


            
###########################################################################
#########                   Algo Path calculation                ##########
###########################################################################
def algoPath(Graph, DAG):
    mergedlist = []
#Première étape du Root
##########

    node = Graph.nodes() # Nodes in aera of Root
    node.remove('Root')
#On va calculer maintenant le + court chemin vers le route pour chacun des noeuds en fcontion d'etx (du poid)
    for i in range(len(node)):   
        table = nx.shortest_path(Graph,source=node[i],target='Root', weight='etx') #calcul de la route la plus courte du noeud vers le root
        DAG.add_edge(node[i], table[1], Graph.get_edge_data(table[1], node[i],default=1)) # Pour chaque voisin on crée le lien dirigé vers le papa
    
###########################################################################
#########    Hybrid Algo Path calculation interface Oriented     ##########  A REVOIR!!!!!
###########################################################################
##def algoHybridPath(Graph, DAG):
##    mergedlist = []
##
###Première étape du Root
############
##
##    voisin_root = Graph.nodes() # Nodes in aera of Root
##    voisin_root.remove('Root')
##
##    all_nodes = Graph.nodes()
##    attrib_node = nx.get_node_attributes(Graph, 'rank')
##    for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
##            #print attrib_node
##            DAG.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])
###On va calculer maintenant le + court chemin vers le route pour chacun des noeuds en fcontion d'etx (du poid)
##    for i in range(len(voisin_root)):   
##        table = nx.shortest_path(Graph,source=voisin_root[i],target='Root', weight='etx') #calcul de la route la plus courte du noeud vers le root
##        attrib = Graph.get_edge_data(table[0],table[1])
##        cle = attrib.keys()
##        if len(cle) > 1:
##                if attrib[cle[0]]['etx'] < attrib[cle[1]]['etx']:  #Best etx is chosen
##                        DAG.add_edge(voisin_root[i], table[1], attrib[cle[0]] ) # Creating edge
##                else:
##                        DAG.add_edge(voisin_root[i], table[1], attrib[cle[1]] ) 
##        else:
##                DAG.add_edge(voisin_root[i], table[1], attrib[cle[0]] ) 
##

#############################################################################


########### New algo ###################################
def algoHybridPath(Graph, DAG):

    all_nodes = Graph.nodes()
    attrib_node = nx.get_node_attributes(Graph, 'rank')
    for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
            #print attrib_node
            DAG.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])



    edge = list(set(Graph.edges())) #on vire les doublons au passage
   # print edge
    for j in range(len(edge)):
            e = edge[j]
            data = Graph.get_edge_data(*e)
            key = data.keys()
            if len(key) > 1:
                    if data[key[0]]['etx'] > data[key[1]]['etx']:
                            DAG.add_edge(e[0],e[1], color = data[key[1]]['color'], key = data[key[1]]['key'] ,etx = data[key[1]]['etx'], mac = data[key[1]]['mac'] ) # Creating edge
                    else:
                            DAG.add_edge(e[0],e[1], color = data[key[0]]['color'], key = data[key[0]]['key'] ,etx = data[key[0]]['etx'], mac = data[key[0]]['mac'] ) # Creating edge
            else:
                    DAG.add_edge(e[0],e[1], color = data[key[0]]['color'] , key = data[key[0]]['key'],etx = data[key[0]]['etx'], mac = data[key[0]]['mac'] ) # Creating edge
        
    #print nx.get_node_attributes(DAG, 'rank')
####
##    voisin_root = Graph.nodes() # Nodes in aera of Root
##    voisin_root.remove('Root')
##
##    for k in range(len(voisin_root)):
##            f = voisin_root[K]
##            data2 = DAG.get_edge_data(*f)
##            key2 = data2.keys()
##            for l in range(len(key2)):
##                
##
##            if len(key2) > 1:
##                    if data2[key[0]
###########################################################################






###########################################################################
#########      Hybrid Algo Path calculation Parent Oriented      ##########
###########################################################################
def algoHybridPath_parent(Graph, graph_po):
 #   mergedlist = []
 #   temp_graph = nx.Graph()
#Première étape on créer un graph ou les multi-liens sont remplacés par un lien "moyen"
##########
    all_nodes = Graph.nodes()
    attrib_node = nx.get_node_attributes(Graph, 'rank')
    for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
            #print attrib_node
            graph_po.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])



    edge = list(set(Graph.edges())) #on vire les doublons au passage
    for j in range(len(edge)):
            e = edge[j]
            data = Graph.get_edge_data(*e)
            key = data.keys()
            if len(key) > 1:
                    etx_m = int(round((data[key[0]]['etx']+data[key[1]]['etx'])/2,0))
            else:
                    etx_m = int(round((data[key[0]]['etx']+9)/2,0))   ## New penalizing formula
            graph_po.add_edge(e[0],e[1], color = 'Black' , key = j ,etx = etx_m , mac = 'none') # Creating edge







#Best parent graph
###########################################

def BestParent(Graph, BGraph):
        
        if policy == 3:   ### Calcul de rang à ajouter
                temp = 65537
                best_parent = 'Empty'
                all_nodes = Graph.nodes()
                attrib_node = nx.get_node_attributes(Graph, 'rank')
                for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
                    BGraph.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])
                all_nodes.remove('Root')
                attrib_node = nx.get_node_attributes(Graph, 'rank')
                for i in range(len(all_nodes)):
                        temp = 65537
                        best_parent = 'Empty' ####NEWWWW
                        success = Graph.successors(all_nodes[i])
                        if success:
                                for j in range(len(success)):
                                        attrib = Graph.get_edge_data(all_nodes[i],success[j], default=0)
                                        if attrib_node[success[j]]+(attrib['etx']*256) < temp:
                                                temp = attrib_node[success[j]]+(attrib['etx']*256)
                                                best_parent = success[j]
                                        
                                attrib2 = Graph.get_edge_data(all_nodes[i],str(best_parent), default=0)
                                if attrib2:
                                        BGraph.add_edge(all_nodes[i],best_parent, color = attrib2['color'], key = attrib2['key'] ,etx = attrib2['etx'], mac = attrib2['mac'] ) # Creating edge

        if policy == 1:   ### Calcul de rang à ajouter
                temp = 65537
                best_parent_PLC = 'Empty'
                best_parent_RF = 'Empty'
                all_nodes = Graph.nodes()
                attrib_node = nx.get_node_attributes(Graph, 'rank')
                for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
                    BGraph.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])
                all_nodes.remove('Root')
                attrib_node = nx.get_node_attributes(Graph, 'rank')
                for i in range(len(all_nodes)):
                        temp = 65537
                        success = Graph.successors(all_nodes[i])
                        if success:
                                for j in range(len(success)):
                                        attrib = Graph.get_edge_data(all_nodes[i],success[j], default=0)
                                        #print attrib
                                        print temp, success[j], attrib['etx']
                                        if attrib_node[success[j]]+(attrib['etx']*256) < temp and attrib['mac'] == 'PLC':
                                                print "PLC test"
                                                temp = attrib_node[success[j]]+(attrib['etx']*256)
                                                best_parent_PLC = success[j]
                                                print "Best P PLC: " ,best_parent_PLC
                                        if attrib_node[success[j]]+(attrib['etx']*256) < temp and attrib['mac'] == 'RF':
                                                print "RF test"
                                                temp = attrib_node[success[j]]+(attrib['etx']*256)
                                                best_parent_RF = success[j]
                                                print "Best P RF: " ,best_parent_RF
                                        temp = 65537
                                if best_parent_PLC == 'Empty':
                                        print "pas de PLC"
                                        best_parent = best_parent_RF
                                else:
                                        print "Atrribution du lien PLC"
                                        best_parent = best_parent_PLC

                                attrib2 = Graph.get_edge_data(all_nodes[i],str(best_parent), default=0)
                                if attrib2:
                                     print all_nodes[i], best_parent
                                     BGraph.add_edge(all_nodes[i],best_parent, color = attrib2['color'] , key = attrib2['key'],etx = attrib2['etx'], mac = attrib2['mac']  ) # Creating edge
                                     print BGraph.edges()
                                best_parent_PLC = 'Empty'
                                best_parent_RF = 'Empty'
                                #temp = 65536

        if policy == 2:   ### Calcul de rang à ajouter
                temp = 65537
                best_parent_PLC = 'Empty'
                best_parent_RF = 'Empty'
                all_nodes = Graph.nodes()
                attrib_node = nx.get_node_attributes(Graph, 'rank')
                for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
                    BGraph.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])
                all_nodes.remove('Root')
                attrib_node = nx.get_node_attributes(Graph, 'rank')
                for i in range(len(all_nodes)):
                        temp = 65537
                        success = Graph.successors(all_nodes[i])
                        if success:
                                for j in range(len(success)):
                                        attrib = Graph.get_edge_data(all_nodes[i],success[j], default=0)
                                        #print attrib
                                        print temp, success[j], attrib['etx']
                                        if attrib_node[success[j]]+(attrib['etx']*256) < temp and attrib['mac'] == 'RF':
                                                print "RF test"
                                                temp = attrib_node[success[j]]+(attrib['etx']*256)
                                                best_parent_RF = success[j]
                                                print "Best P RF: " ,best_parent_RF
                                        if attrib_node[success[j]]+(attrib['etx']*256) < temp and attrib['mac'] == 'PLC':
                                                print "PLC test"
                                                temp = attrib_node[success[j]]+(attrib['etx']*256)
                                                best_parent_PLC = success[j]
                                                print "Best P PLC: " ,best_parent_PLC
                                        
                                        temp = 65537
                                if best_parent_RF == 'Empty':
                                        print "pas de RF"
                                        best_parent = best_parent_PLC
                                else:
                                        print "Atrribution du lien RF"
                                        best_parent = best_parent_RF

                                attrib2 = Graph.get_edge_data(all_nodes[i],str(best_parent), default=0)
                                if attrib2:
                                     print all_nodes[i], best_parent
                                     BGraph.add_edge(all_nodes[i],best_parent, color = attrib2['color'] , key = attrib2['key'],etx = attrib2['etx'], mac = attrib2['mac']  ) # Creating edge
                                     print BGraph.edges()
                                best_parent_PLC = 'Empty'
                                best_parent_RF = 'Empty'


##        if policy == 4:
##                temp = 65536
##                best_parent = 'Empty'
##                all_nodes = Graph.nodes()
##                attrib_node = nx.get_node_attributes(Graph, 'rank')
##                for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
##                    BGraph.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])
##                all_nodes.remove('Root')
##                attrib_node = nx.get_node_attributes(Graph, 'rank')
##                for i in range(len(all_nodes)):
##                        temp = 65536
##                        success = Graph.successors(all_nodes[i])
##                        if success:
##                                for j in range(len(success)):
##                                        attrib = Graph.get_edge_data(all_nodes[i],success[j], default=0)
##                                        if attrib_node[success[j]]+(attrib['etx']*256) < temp:
##                                                temp = attrib_node[success[j]]+(attrib['etx']*256)
##                                                best_parent = success[j]
##                                        
##                                attrib2 = Graph.get_edge_data(all_nodes[i],str(best_parent), default=0)
##                                if attrib2:
##                                        BGraph.add_edge(all_nodes[i],best_parent, color = attrib2['color'] ,etx = attrib2['etx'] ) # Creating edge



#Best parent graph Parent Oriented
###########################################

def BestParent_PO(Graph, BGraph, Multigraph):    
        if policy == 3 or policy == 1 or policy == 2:  ### Calcul de rang à ajouter
 
                temp = 65537
                best_parent = 'Empty'
                all_nodes = Graph.nodes()
                attrib_node = nx.get_node_attributes(Graph, 'rank')
                for i in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
                    BGraph.add_node(all_nodes[i], rank = attrib_node[all_nodes[i]])
                all_nodes.remove('Root')
                attrib_node = nx.get_node_attributes(Graph, 'rank')
                for i in range(len(all_nodes)):
                        temp = 65537
                        best_parent = 'Empty' #####NEWWWWWWWW
                        success = Graph.successors(all_nodes[i])
                        if success:
                                for j in range(len(success)):
                                        attrib = Graph.get_edge_data(all_nodes[i],success[j], default=0)
                                        if attrib_node[success[j]]+(attrib['etx']*256) < temp:
                                                temp = attrib_node[success[j]]+(attrib['etx']*256)
                                                best_parent = success[j]
                                        
                                attrib2 = Multigraph.get_edge_data(all_nodes[i],str(best_parent), default=0)
                                cle = attrib2.keys()
                                if len(attrib2) == 1:
                                        BGraph.add_edge(all_nodes[i],best_parent, color = attrib2[cle[0]]['color'] ,key = cle[0], etx = attrib2[cle[0]]['etx'], mac = attrib2[cle[0]]['mac'] ) # Creating edge
                                if len(attrib2) == 2:
                                
                                        BGraph.add_edge(all_nodes[i],best_parent, color = attrib2[cle[0]]['color'] ,key = cle[0],etx = attrib2[cle[0]]['etx'], mac = attrib2[cle[0]]['mac']  ) # Creating edge
                                        BGraph.add_edge(all_nodes[i],best_parent, color = attrib2[cle[1]]['color'] ,key = cle[1],etx = attrib2[cle[1]]['etx'], mac = attrib2[cle[1]]['mac']  ) # Creating edge
                        



                        
###########################################################################
#Drawing with Pygraphviz

print "Creation of two DAGs..."
two_dags(graph_user, DAG_PLC, DAG_RF)
##DAG_user = nx.DiGraph()
##DAGcreation(graph_user, DAG_user)

DODAG_PLC = nx.DiGraph()

DAGcreation(DAG_PLC, DODAG_PLC)

DODAG_RF = nx.DiGraph()

DAGcreation(DAG_RF, DODAG_RF)

print "Computation of a Hybrid DAG.."
dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 

#Appel des fonction pour passer le graph nx en agraph
#####################################################


#####Essai best parent
DODAG_RF_best = nx.DiGraph()
BestParent(DODAG_RF, DODAG_RF_best)
DODAG_PLC_best = nx.DiGraph()
BestParent(DODAG_PLC, DODAG_PLC_best)

ADAG_PLC = nx.drawing.nx_agraph.to_agraph(DODAG_PLC)
ADAG_RF = nx.drawing.nx_agraph.to_agraph(DODAG_RF)
agraph_user = nx.drawing.nx_agraph.to_agraph(graph_user)
ADAG_Hybrid = nx.drawing.nx_agraph.to_agraph(DAG_Hybrid)
ABEST_DAG_RF = nx.drawing.nx_agraph.to_agraph(DODAG_RF_best)
ABEST_DAG_PLC = nx.drawing.nx_agraph.to_agraph(DODAG_PLC_best)



# Analyse d'un Multigraph pour tracé sous Graphviz
#############################################
def agraphcreation(graph, cgraph):


    cgraph.graph_attr['nodesep']=0.2
    cgraph.graph_attr['ranksep']=0.2
    
    cgraph.edge_attr['color']='black'
    cgraph.node_attr['shape']='circle'
    cgraph.node_attr['fontsize']= 10
    cgraph.node_attr['style']='filled'
    cgraph.node_attr['fillcolor'] = 'blue'
    cgraph.node_attr['fixedsize']= True
    cgraph.node_attr['width']= 0.5


    cgraph.node_attr['nodesep']=0.1
    cgraph.node_attr['ranksep']=0.1

    list_noeuds = list(set(graph.edges())) #on vire les doublons au passage
    for t in range(len(list_noeuds)):
        attrib = graph.get_edge_data(list_noeuds[t][0],list_noeuds[t][1])
        edge = cgraph.get_edge(list_noeuds[t][0],list_noeuds[t][1])
        edge.attr['label'] = attrib['etx']
        edge.attr['fontsize'] = 10
 #       edge.attr['weight'] = 0
        edge.attr['fontcolor'] = attrib['color']
        edge.attr['arrowsize'] = 0.5

    node = cgraph.get_node('Root')
    node.attr['fillcolor'] = 'red'
    node.attr['label'] = 'Root\n256'
   
    all_nodes = graph.nodes()
    all_nodes.remove('Root')
    attrib_node = nx.get_node_attributes(graph, 'rank')
    for i in range(len(all_nodes)):
            node = cgraph.get_node(all_nodes[i])
            
            node.attr['fillcolor'] = 'white'
            j =str(int(attrib_node[all_nodes[i]]))
            node.attr['label'] = 'N. '+str(all_nodes[i])+'\n'+j
    






    
    cgraph.graph_attr.update(rankdir='BT')  # change direction of the layout



#############################################
# Analyse d'un Multigraph Hybrid pour tracé sous Graphviz
#############################################
def aHybridgraphcreation(graph, cgraph):



    cgraph.graph_attr['nodesep']=0.1
    cgraph.graph_attr['ranksep']=0.1

    cgraph.edge_attr['color']='black'
    cgraph.node_attr['shape']='circle'
    cgraph.node_attr['style']='filled'
    cgraph.node_attr['fontsize']= 10
    cgraph.node_attr['fillcolor'] = 'blue'
    cgraph.node_attr['fixedsize']= True
    cgraph.node_attr['width']= 0.5
    cgraph.graph_attr['nodesep']=0.1

 #  list_noeuds = list(set(graph.edges())) #on vire les doublons au passage
    list_noeuds = list(graph.edges()) #on vire les doublons au passage

    for t in range(len(list_noeuds)):
        attrib = graph.get_edge_data(list_noeuds[t][0],list_noeuds[t][1])
        test = attrib.keys()
        for k in range(len(attrib)):
            cle = attrib.keys()
            edge = cgraph.get_edge(list_noeuds[t][0],list_noeuds[t][1], cle[k])
            
            edge.attr['label'] = attrib[cle[k]]['etx']
            edge.attr['fontcolor'] = attrib[cle[k]]['color']
            edge.attr['fontsize']= 10
            edge.attr['arrowsize'] = 0.5
 #           edge.attr['weight'] = 0
    node = cgraph.get_node('Root')
    node.attr['fillcolor'] = 'red'
    node.attr['label'] = 'Root\n256'


    all_nodes = graph.nodes()
    all_nodes.remove('Root')
    attrib_node = nx.get_node_attributes(graph, 'rank')
    for i in range(len(all_nodes)):
            node = cgraph.get_node(all_nodes[i])
            node.attr['fillcolor'] = 'white'
            j =str(int(attrib_node[all_nodes[i]]))
            node.attr['label'] = 'N. '+str(all_nodes[i])+'\n'+j

    
    cgraph.graph_attr.update(rankdir='BT')  # change direction of the layout
#######################




#############################################
# Analyse d'un Multigraph Hybrid pour tracé sous Graphviz for MI solution
#############################################
def aHybridgraphcreationMI(graph, cgraph):



    cgraph.graph_attr['nodesep']=0.1
    cgraph.graph_attr['ranksep']=0.1

    cgraph.edge_attr['color']='black'
    cgraph.node_attr['shape']='circle'
    cgraph.node_attr['style']='filled'
    cgraph.node_attr['fontsize']= 8
    cgraph.node_attr['fillcolor'] = 'blue'
    cgraph.node_attr['fixedsize']= True
    cgraph.node_attr['width']= 0.5
    cgraph.graph_attr['nodesep']=0.1

 #  list_noeuds = list(set(graph.edges())) #on vire les doublons au passage
    list_noeuds = list(graph.edges()) #on vire les doublons au passage

    for t in range(len(list_noeuds)):
        attrib = graph.get_edge_data(list_noeuds[t][0],list_noeuds[t][1])
        test = attrib.keys()
        for k in range(len(attrib)):
            cle = attrib.keys()
            edge = cgraph.get_edge(list_noeuds[t][0],list_noeuds[t][1], cle[k])
            
            edge.attr['label'] = attrib[cle[k]]['etx']
            edge.attr['fontcolor'] = attrib[cle[k]]['color']
            edge.attr['fontsize']= 10
            edge.attr['arrowsize'] = 0.5
 #           edge.attr['weight'] = 0
    node = cgraph.get_node('Root')
    node.attr['fillcolor'] = 'red'
    node.attr['label'] = 'Root\n256'


    all_nodes = graph.nodes()
    all_nodes.remove('Root')
    attrib_node = nx.get_node_attributes(graph, 'rank')
    attrib_node2 = nx.get_node_attributes(graph, 'rank_plc')
    for i in range(len(all_nodes)):
            node = cgraph.get_node(all_nodes[i])
            node.attr['fillcolor'] = 'white'
            j =str(int(attrib_node[all_nodes[i]]))
            k =str(int(attrib_node2[all_nodes[i]]))
            node.attr['label'] = 'N. '+str(all_nodes[i])+'\n'+'rf:'+j+'\n'+'plc:'+k

    
    cgraph.graph_attr.update(rankdir='BT')  # change direction of the layout
#######################

######################################################
##    Policy computation for parent Oriented solution
######################################################

def policy_PO(graph, bgraph):

           all_nodes = graph.nodes()
           attrib_node = nx.get_node_attributes(graph, 'rank')
           for k in range(len(all_nodes)):# On va chercher tous les rangs du graph original pour ajout
                   #print attrib_node
                   bgraph.add_node(all_nodes[k], rank = attrib_node[all_nodes[k]])

           
           edge = list(graph.edges()) #get all edges of graph
           
           temp_edge = list(set(edge))
 
   





           c1 = Counter(edge)
           c2 = Counter(temp_edge)

           double_link = list(c1 - c2)
          
           c3 = Counter(double_link)
           single_link = list(c2 - c3)
           
           
           
##           for i in range(len(edge)):
##                   temp_edge = list(edge)
##                   del temp_edge[i]
##                   for j in range(len(temp_edge)):
##                           attrib = graph.get_edge_data(*edge[i])
##                           if edge[i] == temp_edge[j]:
##                                   
##

           for i in range(len(double_link)):
                   attrib = graph.get_edge_data(*double_link[i])
                   cle = attrib.keys()
                   if attrib[cle[0]]['etx'] > attrib[cle[1]]['etx']:
                           bgraph.add_edge(*double_link[i], color = attrib[cle[1]]['color'] ,key = cle[1],etx = attrib[cle[1]]['etx'], mac = attrib[cle[1]]['mac'] ) # Creating edge
##                                           print "creation edge", edge[i], attrib[1]['etx']
                   else:
                           bgraph.add_edge(*double_link[i], color = attrib[cle[0]]['color'] ,key = cle[0],etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'] ) # Creating edge
##                                           print "creation edge", edge[i], attrib[0]['etx']
##                                   
##
           for j in range(len(single_link)):
                   attrib = graph.get_edge_data(*single_link[j])                         
                   cle = attrib.keys()
                   bgraph.add_edge(*single_link[j], color = attrib[cle[0]]['color'] ,key = cle[0],etx = attrib[cle[0]]['etx'], mac = attrib[cle[0]]['mac'] ) # Creating edge
##                   print "creation edge", edge[i], attrib[0]['etx']


##           edge_final = list(bgraph.edges())
##           print edge_final
##           all_nodes = bgraph.nodes()
##           print all_nodes
                                   




# Statistiques
##################
def link_Q_rate(graph):
        moy = 0
        edge_nb = 0
        edge = list(graph.edges()) #get all edges of graph
        edge_nb = len(edge)
        
        for j in range(len(edge)):
                   attrib = graph.get_edge_data(*edge[j], default=0)
                   moy = moy + attrib['etx']
                    
        moy = round(float(moy)/(edge_nb),2)
        return moy

def link_Q_rate_Multi(mgraph):
        moy = 0
        edge_nb = 0
        edge = list(mgraph.edges()) #get all edges of graph
        edge_nb = len(edge)
        #print edge_nb
        edge = list(set(edge))
        for j in range(len(edge)):
                   attrib = mgraph.get_edge_data(*edge[j], default=0)
                   key = attrib.keys()
                   for i in range(len(attrib)):
                           moy = moy + attrib[key[i]]['etx']

        moy = round(float(moy)/(edge_nb),2)
        return moy

def parent_rate(graph):
        potential_parent = 0
        node = graph.nodes() # Nodes in aera of Root
        node.remove('Root')
        node_nb = len(node)
        for i in range(len(node)):
                success = graph.successors(node[i])
                potential_parent = potential_parent + len(success)


        potential_parent = round(float(potential_parent)/(node_nb),2)
        return potential_parent

def average_hop(graph):
        hops = 0
        path = 0
        max_hops = 0
        min_hops = 8000
        node = graph.nodes() # Nodes in aera of Root
        node.remove('Root')
        node_nb = len(node)
        for i in range(len(node)):
                try:
  
                        path = len(nx.dijkstra_path(graph,node[i],'Root'))
                        
                except nx.NetworkXNoPath:
                        pass
                if (path-1) > max_hops:
                        max_hops = path-1
                if (path-1) < min_hops:
                        min_hops = path-1
                hops = path-1 + hops
        hops = round(float(hops)/(node_nb),2)
        return {'hops': hops, 'min_hops': min_hops, 'max_hops': max_hops}

def rank_avr(graph):
        rank = 0
        avr_rank = 0
        max_rank = 0
        min_rank = 65538
        node = graph.nodes() # Nodes in aera of Root

        node_nb = len(node)
        attrib_node = nx.get_node_attributes(graph, 'rank')
        cle = attrib_node.keys()
        for i in range(node_nb):
                
                
                if attrib_node[cle[i]] > max_rank:
                     
 
                        max_rank = attrib_node[cle[i]]
                if attrib_node[cle[i]] < min_rank:
                        min_rank = attrib_node[cle[i]]
                rank = rank + attrib_node[cle[i]]
        rank = round(float(rank)/(node_nb),2)
        return {'avr_rank': rank, 'min_rank': min_rank, 'max_rank': max_rank}

def rank_avr_mi(graph):
        rank = 0
        avr_rank = 0
        max_rank = 0
        min_rank = 65538
        node = graph.nodes() # Nodes in aera of Root

        node_nb = len(node)
        attrib_node = nx.get_node_attributes(graph, 'rank')
        attrib_node2 = nx.get_node_attributes(graph, 'rank_plc')
        cle = attrib_node.keys()
        for i in range(node_nb):
                
                
                if attrib_node[cle[i]] > max_rank:
                        max_rank = attrib_node[cle[i]]
                if attrib_node2[cle[i]] > max_rank:
                        max_rank = attrib_node2[cle[i]]
                if attrib_node[cle[i]] < min_rank:
                        min_rank = attrib_node[cle[i]]
                if attrib_node2[cle[i]] < min_rank:
                        min_rank = attrib_node2[cle[i]]
                rank = rank + attrib_node[cle[i]] + attrib_node2[cle[i]]
        rank = round(float(rank)/(node_nb*2),2)
        return {'avr_rank': rank, 'min_rank': min_rank, 'max_rank': max_rank}
        

# Appel des fonctions pour creations des graphs sous Graphviz
#############################################################

#agraphcreation(DAG_user, ADAG_user)
if drawing == 'y':
     print "Drawing of generated graphs and DAGs..."
     agraphcreation(DODAG_PLC, ADAG_PLC)
     agraphcreation(DODAG_RF, ADAG_RF)
     agraphcreation(DODAG_RF_best, ABEST_DAG_RF)
     agraphcreation(DODAG_PLC_best, ABEST_DAG_PLC)
     aHybridgraphcreation(graph_user, agraph_user)
     aHybridgraphcreationMI(DAG_Hybrid, ADAG_Hybrid)

################################################
#Algorithms
################################################
print "Parent Oriented solution calculation..."
graph_po_user = nx.Graph()
algoHybridPath_parent(graph_user, graph_po_user)
DODAG_PO = nx.DiGraph()
DAGcreation(graph_po_user, DODAG_PO)
ADODAG_PO = nx.drawing.nx_agraph.to_agraph(DODAG_PO)
agraphcreation(DODAG_PO, ADODAG_PO)


DODAG_PO_best = nx.MultiDiGraph()
BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
ABEST_DAG_PO = nx.drawing.nx_agraph.to_agraph(DODAG_PO_best)
aHybridgraphcreation(DODAG_PO_best, ABEST_DAG_PO)

DODAG_PO_Policy_best = nx.DiGraph()
policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
ABEST_DAG_PO_Policy = nx.drawing.nx_agraph.to_agraph(DODAG_PO_Policy_best)
agraphcreation(DODAG_PO_Policy_best, ABEST_DAG_PO_Policy)

print "Interface Oriented solution calculation..."

DODAG_IO = nx.DiGraph()
DAGcreation2(graph_user, DODAG_IO)
graph_io_user = nx.DiGraph()

ADODAG_IO = nx.drawing.nx_agraph.to_agraph(DODAG_IO)
agraphcreation(DODAG_IO, ADODAG_IO)

DODAG_IO_best = nx.DiGraph()
BestParent(DODAG_IO, DODAG_IO_best)
ABEST_DAG_IO = nx.drawing.nx_agraph.to_agraph(DODAG_IO_best)
agraphcreation(DODAG_IO_best, ABEST_DAG_IO)


# draw
################################################






if drawing == 'y':
        print "PDF generation..."
        
        agraph_user.draw('images/GRAPH_HYBRID.pdf', prog='dot')
        ADAG_PLC.draw('images/DAG_PLC.pdf', prog='dot')
        ADAG_RF.draw('images/DAG_RF.pdf', prog='dot')
        ADAG_Hybrid.draw('images/DAG_HYBRID.pdf', prog='dot')
        ADODAG_PO.draw('images/DAG_Parent_Oriented.pdf', prog='dot')
        ADODAG_IO.draw('images/DAG_Interface_Oriented.pdf', prog='dot')
        ABEST_DAG_RF.draw('images/DAG_RF_Best_Parent.pdf', prog='dot')
        ABEST_DAG_PLC.draw('images/DAG_PLC_Best_Parent.pdf', prog='dot')
        ABEST_DAG_IO.draw('images/DAG_Interface_Oriented_Best_Parent.pdf', prog='dot')
        ABEST_DAG_PO.draw('images/DAG_Parent_Oriented_Best_Parent.pdf', prog='dot')
        ABEST_DAG_PO_Policy.draw('images/DAG_Parent_Oriented_Best_Parent_Policy_OK.pdf', prog='dot')

def test_DAG(DAG):
        testDAG = nx.is_directed_acyclic_graph(DAG)
        if testDAG == False:
            print namestr(DAG, globals()), "is not a Direct Acyclic Graph"
            
def test_DAG_MI(DAG):
        testDAG = nx.is_directed_acyclic_graph(DAG)
        if testDAG == False:
            reponse = "NO"
        elif testDAG == True:
            reponse = "YES"
        return reponse



#attrib_node = nx.get_node_attributes(DODAG_IO_best, 'rank')


test_DAG(DODAG_PLC)
test_DAG(DODAG_RF)
test_DAG(graph_user)
test_DAG(DAG_Hybrid)
test_DAG(DODAG_PO)
test_DAG(DODAG_IO)
test_DAG(DODAG_RF_best)
test_DAG(DODAG_PLC_best)
test_DAG(DODAG_IO_best)
test_DAG(DODAG_PO_best)

print "******Statistics*******"
print "Average of Link Quality for Original graph: ", link_Q_rate_Multi(graph_user)
print "Average of Link Quality for MI DODAG: ", link_Q_rate_Multi(DAG_Hybrid)
print "Average of Link Quality for PO DODAG: ", link_Q_rate(DODAG_PO)
print "Average of Link Quality for IO DODAG: ", link_Q_rate(DODAG_IO)
print "Average size of parent set MI PLC: ",parent_rate(DODAG_PLC) , "RF: ", parent_rate(DODAG_PLC)
print "Average size of parent set for Parent Oriented: ", parent_rate(DODAG_PO)
print "Average size of parent set for Interface Oriented: ", parent_rate(DODAG_IO)
print "Average Hops MI: ", average_hop(DAG_Hybrid)
print "Average Hops PO: ", average_hop(DODAG_PO_Policy_best)
print "Average Hops IO: ", average_hop(DODAG_IO_best)
print "Average Ranks MI: ", rank_avr_mi(DAG_Hybrid)
print "Average Ranks PO: ", rank_avr(DODAG_PO_Policy_best)
print "Average Ranks IO: ", rank_avr(DODAG_IO_best)#['avr_rank']

##
##end = 0
##while end < 5: #Ici mettre tant qu'on est en dessous de 10% (nb/10)
##     node= int(round(np.random.uniform(1,10), 0))
##     if int(round(np.random.uniform(0,1), 0)) == 0:
##          choix = 'PLC'
##     else:
##          choix = 'RF'
##     
##     edge_list = graph_user.edges(str(node), 'mac')
##     for i in range(len(edge_list)):
##          if edge_list[i][2] == choix:
##               attrib =  graph_user.get_edge_data(edge_list[i][0], edge_list[i][1])
##               cle = attrib.keys()
##               if attrib[cle[0]]['mac'] == choix:
##                    graph_user.remove_edge(edge_list[i][0], edge_list[i][1],key=cle[0])
##               else:
##                    graph_user.remove_edge(edge_list[i][0], edge_list[i][1],key=cle[1])
##          two_dags(graph_user, DAG_PLC, DAG_RF)
##          DODAG_PLC = nx.DiGraph()
##          DAGcreation(DAG_PLC, DODAG_PLC)
##          DODAG_RF = nx.DiGraph()
##          DAGcreation(DAG_RF, DODAG_RF)
##          dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
##          DODAG_RF_best = nx.DiGraph()
##          BestParent(DODAG_RF, DODAG_RF_best)
##          DODAG_PLC_best = nx.DiGraph()
##          BestParent(DODAG_PLC, DODAG_PLC_best)
##          graph_po_user = nx.Graph()
##          algoHybridPath_parent(graph_user, graph_po_user)
##          DODAG_PO = nx.DiGraph()
##          DAGcreation(graph_po_user, DODAG_PO)
##          DODAG_PO_best = nx.MultiDiGraph()
##          BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
##          DODAG_PO_Policy_best = nx.DiGraph()
##          policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
##          DODAG_IO = nx.DiGraph()
##          DAGcreation2(graph_user, DODAG_IO)
##          DODAG_IO_best = nx.DiGraph()
##          BestParent(DODAG_IO, DODAG_IO_best)
##          attrib_node = nx.get_node_attributes(DODAG_IO_best, 'rank')
##          key = attrib_node.keys()
##          end = 0
##          for j in range(len(attrib_node)):
##               if attrib_node[key[j]] == 65536:
##                    end = end +1
          
               
               
               
                         



###IMPORTANT
##print DODAG_IO_best.successors('8')
##if DODAG_IO_best.successors('8') == []:
##
##     print "Hello "
##print DODAG_IO.successors('8')
##
##print DODAG_IO_best.successors('4')
##if DODAG_IO_best.successors('4') == []:
##
##     print "Hello "
############################################################################

############################################################################

############################################################################
##def sim_tour():
##     DAG_PLC = nx.Graph()
##     DAG_RF = nx.Graph()
##     DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
##     graph_user = nx.MultiGraph() # Undirected graph creation
##     graph_random_2(node_number)
##     two_dags(graph_user, DAG_PLC, DAG_RF)
##     DODAG_PLC = nx.DiGraph()
##     DAGcreation(DAG_PLC, DODAG_PLC)
##     DODAG_RF = nx.DiGraph()
##     DAGcreation(DAG_RF, DODAG_RF)
##     dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
##     DODAG_RF_best = nx.DiGraph()
##     BestParent(DODAG_RF, DODAG_RF_best)
##     DODAG_PLC_best = nx.DiGraph()
##     BestParent(DODAG_PLC, DODAG_PLC_best)
##     graph_po_user = nx.Graph()
##     algoHybridPath_parent(graph_user, graph_po_user)
##     DODAG_PO = nx.DiGraph()
##     DAGcreation(graph_po_user, DODAG_PO)
##     DODAG_PO_best = nx.MultiDiGraph()
##     BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
##     DODAG_PO_Policy_best = nx.DiGraph()
##     policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
##     DODAG_IO = nx.DiGraph()
##     DAGcreation2(graph_user, DODAG_IO)
##     DODAG_IO_best = nx.DiGraph()
##     BestParent(DODAG_IO, DODAG_IO_best)
##
##     c.writerow([i, node_number, link_Q_rate_Multi(graph_user), link_Q_rate(DODAG_PO_Policy_best), link_Q_rate(DODAG_IO_best), parent_rate(DODAG_PO), parent_rate(DODAG_IO), average_hop(DODAG_PO_Policy_best), average_hop(DODAG_IO_best), rank_avr(DODAG_PO_Policy_best), rank_avr(DODAG_IO_best), test_DAG_MI(DAG_Hybrid)]) 
############################################################################

############################################################################

############################################################################
fichier ={
        '100': ['case100', 'stats100.csv', 100, 'Case 100: '],
        '200': ['case200', 'stats200.csv', 200, 'Case 200: '],
        '300': ['case300', 'stats300.csv', 300, 'Case 300: '],
        '400': ['case400', 'stats400.csv', 400, 'Case 400: '],
        '500': ['case500', 'stats500.csv', 500, 'Case 500: '],
        '600': ['case600', 'stats600.csv', 600, 'Case 600: '],
        '700': ['case700', 'stats700.csv', 700, 'Case 700: '],
        '800': ['case800', 'stats800.csv', 800, 'Case 800: '],
        '900': ['case900', 'stats900.csv', 900, 'Case 900: '],
        '1000': ['case1000', 'stats1000.csv', 1000, 'Case 1000: '],

    }


def simul(w):
     global graph_user, DAG_PLC, DAG_RF, DAG_Hybrid
     ##
     ##############################################################################
     ##############################################################################
     ##############################################################################
     with open(str(fichier[str((w+1)*100)][1]), "wb") as file:
          fichier[str((w+1)*100)][0] = csv.writer(file)
          fichier[str((w+1)*100)][0] .writerow(["Case","Node_NB", "LQ_Original","LQ_PO","LQ_IO","PP_PO","PP_IO","AVR_HOPS_PO","MIN_HOPS_PO","MAX_HOPS_PO","AVR_HOPS_IO","MIN_HOPS_IO","MAX_HOPS_IO","AVR_RANK_PO","MIN_RANK_PO","MAX_RANK_PO","AVR_RANK_IO","MIN_RANK_IO","MAX_RANK_IO", "DAG_MI_OK","Parent_change_in_set_PO", "Parent_change_new_PO","No_Parent_PO","Parent_change_in_set_IO", "Parent_change_new_IO","No_Parent_IO","Parent_change_in_set_MI", "Parent_change_new_MI","No_Parent_MI"])
          end_mi = 0
          end_io = 0
          end_po = 0
          nb_table_PO = {}
          nb_table_IO = {}
          nb_table_MI_PLC = {}
          nb_table_MI_RF = {}
          nb_table_PO_old = {}
          nb_table_IO_old = {}
          nb_table_MI_PLC_old = {}
          nb_table_MI_RF_old = {}

          
          for i in range(10):##100 à remettre!!!
               parent_change_in_set_PO = 0
               parent_change_not_set_PO = 0
               orphan_node_PO = 0
               parent_change_in_set_IO = 0
               parent_change_not_set_IO = 0
               orphan_node_IO = 0
               parent_change_in_set_MI_PLC = 0
               parent_change_not_set_MI_PLC = 0
               orphan_node_MI_PLC = 0
               parent_change_in_set_MI_RF = 0
               parent_change_not_set_MI_RF = 0
               orphan_node_MI_RF = 0
               link_nb_rm =0
               link_nb_rm_1 =0
               link_nb_rm_mi =0
               link_nb_rm_mi_1 =0
               link_nb_rm_po =0
               link_nb_rm_po_1 =0
               link_nb_rm_io =0
               link_nb_rm_io_1 =0
               DAG_PLC = nx.Graph()
               DAG_RF = nx.Graph()
               DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
               graph_user = nx.MultiGraph() # Undirected graph creation
               graph_random_2(fichier[str((w+1)*100)][2] )
               two_dags(graph_user, DAG_PLC, DAG_RF)
               DODAG_PLC = nx.DiGraph()
               DAGcreation(DAG_PLC, DODAG_PLC)
               DODAG_RF = nx.DiGraph()
               DAGcreation(DAG_RF, DODAG_RF)
               dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
               DODAG_RF_best = nx.DiGraph()
               BestParent(DODAG_RF, DODAG_RF_best)
               DODAG_PLC_best = nx.DiGraph()
               BestParent(DODAG_PLC, DODAG_PLC_best)
               graph_po_user = nx.Graph()
               algoHybridPath_parent(graph_user, graph_po_user)
               DODAG_PO = nx.DiGraph()
               DAGcreation(graph_po_user, DODAG_PO)
               DODAG_PO_best = nx.MultiDiGraph()
               BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
               DODAG_PO_Policy_best = nx.DiGraph()
               policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
               DODAG_IO = nx.DiGraph()
               DAGcreation2(graph_user, DODAG_IO)
               DODAG_IO_best = nx.DiGraph()
               BestParent(DODAG_IO, DODAG_IO_best)

               ########### Creation des NB table
               for u in range(fichier[str((w+1)*100)][2]-1):
                    nb_po_temp_PP = {}
                    nb_po_temp_IO = {}
                    nb_po_temp_MI_PLC = {}
                    nb_po_temp_MI_RF = {}
                    nb_po_temp_PP['P_S'] = DODAG_PO.successors(str(u+1))
                    nb_po_temp_PP['P_P'] = DODAG_PO_best.successors(str(u+1))
                    nb_po_temp_IO['P_S'] = DODAG_IO.successors(str(u+1))
                    nb_po_temp_IO['P_P'] = DODAG_IO_best.successors(str(u+1))
                    nb_po_temp_MI_PLC['P_S'] = DODAG_PLC.successors(str(u+1))
                    nb_po_temp_MI_PLC['P_P'] = DODAG_PLC_best.successors(str(u+1))
                    nb_po_temp_MI_RF['P_S'] = DODAG_RF.successors(str(u+1))
                    nb_po_temp_MI_RF['P_P'] = DODAG_RF_best.successors(str(u+1))
                    nb_table_PO_old[str(u+1)] = copy.deepcopy(nb_po_temp_PP)
                    nb_table_IO_old[str(u+1)] = copy.deepcopy(nb_po_temp_IO)
                    nb_table_MI_PLC_old[str(u+1)] = copy.deepcopy(nb_po_temp_MI_PLC)
                    nb_table_MI_RF_old[str(u+1)] = copy.deepcopy(nb_po_temp_MI_RF)

               
               
     #####################
     ### ESSAI Sur DODAG_IO
               end_mi = 0
               end_io = 0
               end_po = 0
               condition_mi = 0
               condition_po = 0
               condition_io = 0
               condition_mi2 = 0
               condition_po2 = 0
               condition_io2 = 0
               while ( end_io < (fichier[str((w+1)*100)][2]/10) or end_po < (fichier[str((w+1)*100)][2]/10) or end_mi < (fichier[str((w+1)*100)][2]/10)): #Ici mettre tant qu'on est en dessous de 10% (nb/10)  # end_mi < 10 or end_po < 10 or
                    node= int(round(np.random.uniform(1,fichier[str((w+1)*100)][2]), 27))
                    if int(round(np.random.uniform(0,1), 0)) == 0:
                         choix = 'PLC'
                    else:
                         choix = 'RF'
                    edge_list = graph_user.edges(str(node), 'mac')
                    for k in range(len(edge_list)):
                         if edge_list[k][2] == choix:
                              attrib =  graph_user.get_edge_data(edge_list[k][0], edge_list[k][1])
                              
                              
                              cle = attrib.keys()
                              
                              
                              if attrib[cle[0]]['mac'] == choix:
                                   try:
                                        DODAG_IO.remove_edge(edge_list[k][0], edge_list[k][1])#,key=cle[0])
                                   except nx.exception.NetworkXError:
                                        pass
                                   try:
                                        DODAG_IO.remove_edge(edge_list[k][1], edge_list[k][0])#,key=cle[0])
                                   except nx.exception.NetworkXError:
                                        pass
                                   if choix == 'PLC':
                                        try:
                                             DAG_PLC.remove_edge(edge_list[k][0], edge_list[k][1])#,key=cle[0])
                                        except nx.exception.NetworkXError:
                                             pass
                                        try:
                                             DAG_PLC.remove_edge(edge_list[k][1], edge_list[k][0])#,key=cle[0])
                                        except nx.exception.NetworkXError:
                                             pass
                                   else:
                                        try:
                                             DAG_RF.remove_edge(edge_list[k][0], edge_list[k][1])#,key=cle[0])
                                        except nx.exception.NetworkXError:
                                             pass
                                        try:
                                             DAG_RF.remove_edge(edge_list[k][1], edge_list[k][0])#,key=cle[0])
                                        except nx.exception.NetworkXError:
                                             pass
                                   
                              if len(attrib) > 1:
                                   if attrib[cle[1]]['mac'] == choix:
                                        try:
                                             DODAG_IO.remove_edge(edge_list[k][0], edge_list[k][1])#,key=cle[0])
                  
                                        except nx.exception.NetworkXError:
                                             pass
                                        try:
                                             DODAG_IO.remove_edge(edge_list[k][1], edge_list[k][0])#,key=cle[0])
                                             
                                        except nx.exception.NetworkXError:
                                             pass
                                        if choix == 'PLC':
                                             try:
                                                  DODAG_PLC.remove_edge(edge_list[k][0], edge_list[k][1])#,key=cle[0])
                                             except nx.exception.NetworkXError:
                                                  pass
                                             try:
                                                  DODAG_PLC.remove_edge(edge_list[k][1], edge_list[k][0])#,key=cle[0])
                                             except nx.exception.NetworkXError:
                                                  pass
                                        else:
                                             try:
                                                  DODAG_RF.remove_edge(edge_list[k][0], edge_list[k][1])#,key=cle[0])
                                             except nx.exception.NetworkXError:
                                                  pass
                                             try:
                                                  DODAG_RF.remove_edge(edge_list[k][1], edge_list[k][0])#,key=cle[0])
                                             except nx.exception.NetworkXError:
                                                  pass
                              attrib_po = DODAG_PO.get_edge_data(edge_list[k][0],edge_list[k][1], default=0)
                              e = (edge_list[k][0],edge_list[k][1])
                              if attrib_po == 0:
                                   attrib_po = DODAG_PO.get_edge_data(edge_list[k][1],edge_list[k][0], default=0)
                                   e = (edge_list[k][1],edge_list[k][0])

                              if attrib_po != 0 and len(attrib) == 1:
                                   
     ##                              etx_m = int(round((attrib[cle[0]]['etx']+9)/2,0))   ## New penalizing formula
     ##                              DODAG_PO.add_edge(*e, color = 'Black' , key = attrib_po['key'] ,etx = etx_m , mac = 'none') # Creating edge
                                   if attrib[cle[0]]['mac'] == choix:
                                             graph_user.remove_edge(edge_list[k][0], edge_list[k][1], cle[0])
                                             link_nb_rm = link_nb_rm +1
                                   DODAG_PO.remove_edge(*e)
                                   DODAG_PO_copy = DODAG_PO.to_undirected()
                                   DODAG_PO = nx.DiGraph()
                                   for z in range ((fichier[str((w+1)*100)][2])-1):
                                        DODAG_PO_copy.add_node(str(z+1), pos=(5, 13), rank = 65536)
                                   DAGcreation(DODAG_PO_copy, DODAG_PO)
                                   DODAG_PO_best = nx.MultiDiGraph()
                                   DODAG_PO_best = nx.MultiDiGraph()
                                   BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
                                   DODAG_PO_Policy_best = nx.DiGraph()
                                   policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)

                              if ((attrib_po != 0) and (len(attrib) > 1)):
                                   
                                   if attrib[cle[0]]['mac'] == choix:
                                        graph_user.remove_edge(edge_list[k][0], edge_list[k][1], cle[0])
                                        link_nb_rm = link_nb_rm +1

                                   else:
                                        graph_user.remove_edge(edge_list[k][0], edge_list[k][1], cle[1])
                                        link_nb_rm = link_nb_rm +1
      
                         DODAG_IO_copy = DODAG_IO.to_undirected()
                         DODAG_IO = nx.DiGraph()
                         for z in range ((fichier[str((w+1)*100)][2])-1):
                              DODAG_IO_copy.add_node(str(z+1), pos=(5, 13), rank = 65536)
                         DAGcreation(DODAG_IO_copy, DODAG_IO)
                         DODAG_IO_best = nx.DiGraph()
                         BestParent(DODAG_IO, DODAG_IO_best)

                         attrib_node = nx.get_node_attributes(DODAG_PO_best, 'rank')
                         key = attrib_node.keys()
                         end_po = 0
                         for j in range(len(attrib_node)):
                              if attrib_node[key[j]] == 65536:
                                   end_po = end_po +1

                         attrib_node = nx.get_node_attributes(DODAG_IO_best, 'rank')
                         key = attrib_node.keys()
                         end_io = 0
                         for j in range(len(attrib_node)):
                              if attrib_node[key[j]] == 65536:
                                   end_io = end_io +1

                         #############MI
                         if choix == 'PLC':
                              DODAG_PLC_copy = DODAG_PLC.to_undirected()
                              DODAG_PLC = nx.DiGraph()
                              for z in range ((fichier[str((w+1)*100)][2])-1):
                                        DODAG_PLC.add_node(str(z+1), pos=(5, 13), rank = 65536)
                              DAGcreation(DAG_PLC, DODAG_PLC)
                              DODAG_PLC_best = nx.DiGraph()
                              BestParent(DODAG_PLC, DODAG_PLC_best)
                         else:
                              DODAG_RF_copy = DODAG_RF.to_undirected()
                              DODAG_RF = nx.DiGraph()
                              for z in range ((fichier[str((w+1)*100)][2])-1):
                                        DODAG_RF.add_node(str(z+1), pos=(5, 13), rank = 65536)
                              DAGcreation(DAG_RF, DODAG_RF)
                              DODAG_RF_best = nx.DiGraph()
                              BestParent(DODAG_RF, DODAG_RF_best)
      




                              
                         #################fin MI
                         if choix == 'PLC': 
                              attrib_node = nx.get_node_attributes(DODAG_PLC, 'rank')
                              key = attrib_node.keys()
                              end_mi = 0
                              for j in range(len(attrib_node)):
                                   if attrib_node[key[j]] == 65536:
                                        end_mi = end_mi +1
                         if choix == 'RF': 
                              attrib_node = nx.get_node_attributes(DODAG_RF, 'rank')
                              key = attrib_node.keys()
                              end_mi = 0
                              for j in range(len(attrib_node)):
                                   if attrib_node[key[j]] == 65536:
                                        end_mi = end_mi +1
                         ########### Creation des NB table
                         for u in range((fichier[str((w+1)*100)][2])-1):
                              nb_po_temp_PP = {}
                              nb_po_temp_IO = {}
                              nb_po_temp_MI_PLC = {}
                              nb_po_temp_MI_RF = {}
                              nb_po_temp_PP['P_S'] = DODAG_PO.successors(str(u+1))
                              nb_po_temp_PP['P_P'] = DODAG_PO_best.successors(str(u+1))
                              nb_po_temp_IO['P_S'] = DODAG_IO.successors(str(u+1))
                              nb_po_temp_IO['P_P'] = DODAG_IO_best.successors(str(u+1))
                              nb_po_temp_MI_PLC['P_S'] = DODAG_PLC.successors(str(u+1))
                              nb_po_temp_MI_PLC['P_P'] = DODAG_PLC_best.successors(str(u+1))
                              nb_po_temp_MI_RF['P_S'] = DODAG_RF.successors(str(u+1))
                              nb_po_temp_MI_RF['P_P'] = DODAG_RF_best.successors(str(u+1))
                              nb_table_PO[str(u+1)] = copy.deepcopy(nb_po_temp_PP)
                              nb_table_IO[str(u+1)] = copy.deepcopy(nb_po_temp_IO)
                              nb_table_MI_PLC[str(u+1)] = copy.deepcopy(nb_po_temp_MI_PLC)
                              nb_table_MI_RF[str(u+1)] = copy.deepcopy(nb_po_temp_MI_RF)

                              
                         for u in range((fichier[str((w+1)*100)][2])-1):
                              
                              if nb_table_IO[str(u+1)]['P_P'] == [] and nb_table_IO_old[str(u+1)]['P_P'] != []:
                                   orphan_node_IO = orphan_node_IO +1
                              elif nb_table_IO[str(u+1)]['P_P'] == [] :
                                   pass
                                   
                              else:
                                   if nb_table_IO[str(u+1)]['P_P'][0] != nb_table_IO_old[str(u+1)]['P_P'][0]:


                                        if nb_table_IO[str(u+1)]['P_P'][0] in nb_table_IO_old[str(u+1)]['P_S']:
                                             parent_change_in_set_IO = parent_change_in_set_IO + 1
                                        else:
                                             parent_change_not_set_IO = parent_change_not_set_IO +1


                         for u in range((fichier[str((w+1)*100)][2])-1):
                              
                              if nb_table_PO[str(u+1)]['P_P'] == [] and nb_table_PO_old[str(u+1)]['P_P'] != []:
                                   orphan_node_PO = orphan_node_PO +1
                              elif nb_table_PO[str(u+1)]['P_P'] == [] :
                                   pass
                                   
                              else:
                                   if nb_table_PO[str(u+1)]['P_P'][0] != nb_table_PO_old[str(u+1)]['P_P'][0]:


                                        if nb_table_PO[str(u+1)]['P_P'][0] in nb_table_PO_old[str(u+1)]['P_S']:
                                             parent_change_in_set_PO = parent_change_in_set_PO + 1
                                        else:
                                             parent_change_not_set_PO = parent_change_not_set_PO +1


                         for u in range((fichier[str((w+1)*100)][2])-1):
                              
                              if nb_table_MI_PLC[str(u+1)]['P_P'] == [] and nb_table_MI_PLC_old[str(u+1)]['P_P'] != []:
                                   orphan_node_MI_PLC = orphan_node_MI_PLC +1
                              elif nb_table_MI_PLC[str(u+1)]['P_P'] == [] :
                                   pass
                                   
                              else:
                                   if nb_table_MI_PLC[str(u+1)]['P_P'][0] != nb_table_MI_PLC_old[str(u+1)]['P_P'][0]:


                                        if nb_table_MI_PLC[str(u+1)]['P_P'][0] in nb_table_MI_PLC_old[str(u+1)]['P_S']:
                                             parent_change_in_set_MI_PLC = parent_change_in_set_MI_PLC + 1
                                        else:
                                             parent_change_not_set_MI_PLC = parent_change_not_set_MI_PLC +1

                         for u in range((fichier[str((w+1)*100)][2])-1):
                              
                              if nb_table_MI_RF[str(u+1)]['P_P'] == [] and nb_table_MI_RF_old[str(u+1)]['P_P'] != []:
                                   orphan_node_MI_RF = orphan_node_MI_RF +1
                              elif nb_table_MI_RF[str(u+1)]['P_P'] == [] :
                                   pass
                                   
                              else:
                                   if nb_table_MI_RF[str(u+1)]['P_P'][0] != nb_table_MI_RF_old[str(u+1)]['P_P'][0]:


                                        if nb_table_MI_RF[str(u+1)]['P_P'][0] in nb_table_MI_RF_old[str(u+1)]['P_S']:
                                             parent_change_in_set_MI_RF = parent_change_in_set_MI_RF + 1
                                        else:
                                             parent_change_not_set_MI_RF = parent_change_not_set_MI_RF +1


                         nb_table_PO_old = {}
                         nb_table_IO_old = {}
                         nb_table_MI_PLC_old = {}
                         nb_table_MI_RF_old = {}
                         
                         nb_table_PO_old = copy.deepcopy(nb_table_PO)
                         nb_table_IO_old = copy.deepcopy(nb_table_IO)
                         nb_table_MI_PLC_old = copy.deepcopy(nb_table_MI_PLC)
                         nb_table_MI_RF_old = copy.deepcopy(nb_table_MI_RF)
                         
                         


                    if (end_mi > 0 and condition_mi  == 0) :
                         link_nb_rm_mi_1 = link_nb_rm
                         condition_mi =1
                    if (end_mi >= 10 and condition_mi2  == 0) :
                         link_nb_rm_mi = link_nb_rm
                         condition_mi2 =1
                    if (end_po > 0 and condition_po  == 0) :
                         link_nb_rm_po_1 = link_nb_rm
                         condition_po =1
                    if (end_po >= 10 and condition_po2  == 0) :
                         link_nb_rm_po = link_nb_rm
                         condition_po2 =1
                    if (end_io> 0 and condition_io  == 0) :
                         link_nb_rm_io_1 = link_nb_rm
                         condition_io =1
                    if (end_io >= 10 and condition_io2  == 0) :
                         link_nb_rm_io = link_nb_rm
                         condition_io2 =1
     ##               print end_io
     ##               print end_po
     ##               print end_mi
               

               


               print "Nombre liens retirés MI", link_nb_rm_mi
               print "Nombre liens retirés 1er orphelin MI", link_nb_rm_mi_1
               print "Nombre liens retirés PO", link_nb_rm_po
               print "Nombre liens retirés 1er orphelin PO", link_nb_rm_po_1
               print "Nombre liens retirés IO", link_nb_rm_io
               print "Nombre liens retirés 1er orphelin IO", link_nb_rm_io_1
               print "Nombre de changement de parent dans parent set IO: ", parent_change_in_set_IO
               print "Nombre de changement de parent hors parent set IO: ",parent_change_not_set_IO
               print "Nombre d'orphelins IO: ",orphan_node_IO
               print "Nombre de changement de parent dans parent set PO: ", parent_change_in_set_PO
               print "Nombre de changement de parent hors parent set PO: ",parent_change_not_set_PO
               print "Nombre d'orphelins PO: ",orphan_node_PO
               print "Nombre de changement de parent dans parent set MI : ", parent_change_in_set_MI_PLC+parent_change_in_set_MI_RF
               print "Nombre de changement de parent hors parent set MI : ",parent_change_not_set_MI_PLC+parent_change_not_set_MI_RF
               print "Nombre d'orphelins MI: ",orphan_node_MI_PLC+orphan_node_MI_RF






     ######################
               print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), str(fichier[str((w+1)*100)][3]), i)
               fichier[str((w+1)*100)][0].writerow([i+1, fichier[str((w+1)*100)][2], link_Q_rate_Multi(graph_user), link_Q_rate(DODAG_PO_Policy_best), link_Q_rate(DODAG_IO_best), parent_rate(DODAG_PO), parent_rate(DODAG_IO), average_hop(DODAG_PO_Policy_best)['hops'], average_hop(DODAG_PO_Policy_best)['min_hops'], average_hop(DODAG_PO_Policy_best)['max_hops'], average_hop(DODAG_IO_best)['hops'], average_hop(DODAG_IO_best)['min_hops'], average_hop(DODAG_IO_best)['max_hops'], rank_avr(DODAG_PO_Policy_best)['avr_rank'], rank_avr(DODAG_PO_Policy_best)['min_rank'], rank_avr(DODAG_PO_Policy_best)['max_rank'], rank_avr(DODAG_IO_best)['avr_rank'], rank_avr(DODAG_IO_best)['min_rank'], rank_avr(DODAG_IO_best)['max_rank'], test_DAG_MI(DAG_Hybrid),parent_change_in_set_PO, parent_change_not_set_PO, orphan_node_PO, parent_change_in_set_IO, parent_change_not_set_IO, orphan_node_IO, parent_change_in_set_MI_PLC+parent_change_in_set_MI_RF, parent_change_not_set_MI_PLC+parent_change_not_set_MI_RF, orphan_node_MI_PLC+orphan_node_MI_RF]) 
               

          
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##with open("stats200.csv", "wb") as file:
##     case200 = csv.writer(file)
##     case200.writerow(["Case","Node_NB", "LQ_Original","LQ_PO","LQ_IO","PP_PO","PP_IO","AVR_HOPS_PO","MIN_HOPS_PO","MAX_HOPS_PO","AVR_HOPS_IO","MIN_HOPS_IO","MAX_HOPS_IO","AVR_RANK_PO","MIN_RANK_PO","MAX_RANK_PO","AVR_RANK_IO","MIN_RANK_IO","MAX_RANK_IO", "DAG_MI_OK"])
##
##     for i in range(100):
##          DAG_PLC = nx.Graph()
##          DAG_RF = nx.Graph()
##          DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
##          graph_user = nx.MultiGraph() # Undirected graph creation
##          graph_random_2(200)
##          two_dags(graph_user, DAG_PLC, DAG_RF)
##          DODAG_PLC = nx.DiGraph()
##          DAGcreation(DAG_PLC, DODAG_PLC)
##          DODAG_RF = nx.DiGraph()
##          DAGcreation(DAG_RF, DODAG_RF)
##          dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
##          DODAG_RF_best = nx.DiGraph()
##          BestParent(DODAG_RF, DODAG_RF_best)
##          DODAG_PLC_best = nx.DiGraph()
##          BestParent(DODAG_PLC, DODAG_PLC_best)
##          graph_po_user = nx.Graph()
##          algoHybridPath_parent(graph_user, graph_po_user)
##          DODAG_PO = nx.DiGraph()
##          DAGcreation(graph_po_user, DODAG_PO)
##          DODAG_PO_best = nx.MultiDiGraph()
##          BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
##          DODAG_PO_Policy_best = nx.DiGraph()
##          policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
##          DODAG_IO = nx.DiGraph()
##          DAGcreation2(graph_user, DODAG_IO)
##          DODAG_IO_best = nx.DiGraph()
##          BestParent(DODAG_IO, DODAG_IO_best)
##          print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), "Case 200 : ", i)
##          case200.writerow([i+1, node_number, link_Q_rate_Multi(graph_user), link_Q_rate(DODAG_PO_Policy_best), link_Q_rate(DODAG_IO_best), parent_rate(DODAG_PO), parent_rate(DODAG_IO), average_hop(DODAG_PO_Policy_best)['hops'], average_hop(DODAG_PO_Policy_best)['min_hops'], average_hop(DODAG_PO_Policy_best)['max_hops'], average_hop(DODAG_IO_best)['hops'], average_hop(DODAG_IO_best)['min_hops'], average_hop(DODAG_IO_best)['max_hops'], rank_avr(DODAG_PO_Policy_best)['avr_rank'], rank_avr(DODAG_PO_Policy_best)['min_rank'], rank_avr(DODAG_PO_Policy_best)['max_rank'], rank_avr(DODAG_IO_best)['avr_rank'], rank_avr(DODAG_IO_best)['min_rank'], rank_avr(DODAG_IO_best)['max_rank'], test_DAG_MI(DAG_Hybrid)]) 
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##with open("stats300.csv", "wb") as file:
##     case300 = csv.writer(file)
##     case300.writerow(["Case","Node_NB", "LQ_Original","LQ_PO","LQ_IO","PP_PO","PP_IO","AVR_HOPS_PO","MIN_HOPS_PO","MAX_HOPS_PO","AVR_HOPS_IO","MIN_HOPS_IO","MAX_HOPS_IO","AVR_RANK_PO","MIN_RANK_PO","MAX_RANK_PO","AVR_RANK_IO","MIN_RANK_IO","MAX_RANK_IO", "DAG_MI_OK"])
##
##     for i in range(100):
##          DAG_PLC = nx.Graph()
##          DAG_RF = nx.Graph()
##          DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
##          graph_user = nx.MultiGraph() # Undirected graph creation
##          graph_random_2(300)
##          two_dags(graph_user, DAG_PLC, DAG_RF)
##          DODAG_PLC = nx.DiGraph()
##          DAGcreation(DAG_PLC, DODAG_PLC)
##          DODAG_RF = nx.DiGraph()
##          DAGcreation(DAG_RF, DODAG_RF)
##          dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
##          DODAG_RF_best = nx.DiGraph()
##          BestParent(DODAG_RF, DODAG_RF_best)
##          DODAG_PLC_best = nx.DiGraph()
##          BestParent(DODAG_PLC, DODAG_PLC_best)
##          graph_po_user = nx.Graph()
##          algoHybridPath_parent(graph_user, graph_po_user)
##          DODAG_PO = nx.DiGraph()
##          DAGcreation(graph_po_user, DODAG_PO)
##          DODAG_PO_best = nx.MultiDiGraph()
##          BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
##          DODAG_PO_Policy_best = nx.DiGraph()
##          policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
##          DODAG_IO = nx.DiGraph()
##          DAGcreation2(graph_user, DODAG_IO)
##          DODAG_IO_best = nx.DiGraph()
##          BestParent(DODAG_IO, DODAG_IO_best)
##          print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), "Case 300 : ", i)
##          case300.writerow([i+1, node_number, link_Q_rate_Multi(graph_user), link_Q_rate(DODAG_PO_Policy_best), link_Q_rate(DODAG_IO_best), parent_rate(DODAG_PO), parent_rate(DODAG_IO), average_hop(DODAG_PO_Policy_best)['hops'], average_hop(DODAG_PO_Policy_best)['min_hops'], average_hop(DODAG_PO_Policy_best)['max_hops'], average_hop(DODAG_IO_best)['hops'], average_hop(DODAG_IO_best)['min_hops'], average_hop(DODAG_IO_best)['max_hops'], rank_avr(DODAG_PO_Policy_best)['avr_rank'], rank_avr(DODAG_PO_Policy_best)['min_rank'], rank_avr(DODAG_PO_Policy_best)['max_rank'], rank_avr(DODAG_IO_best)['avr_rank'], rank_avr(DODAG_IO_best)['min_rank'], rank_avr(DODAG_IO_best)['max_rank'], test_DAG_MI(DAG_Hybrid)]) 
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##with open("stats400.csv", "wb") as file:
##     case400 = csv.writer(file)
##     case400.writerow(["Case","Node_NB", "LQ_Original","LQ_PO","LQ_IO","PP_PO","PP_IO","AVR_HOPS_PO","MIN_HOPS_PO","MAX_HOPS_PO","AVR_HOPS_IO","MIN_HOPS_IO","MAX_HOPS_IO","AVR_RANK_PO","MIN_RANK_PO","MAX_RANK_PO","AVR_RANK_IO","MIN_RANK_IO","MAX_RANK_IO", "DAG_MI_OK"])
##
##     for i in range(100):
##          DAG_PLC = nx.Graph()
##          DAG_RF = nx.Graph()
##          DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
##          graph_user = nx.MultiGraph() # Undirected graph creation
##          graph_random_2(400)
##          two_dags(graph_user, DAG_PLC, DAG_RF)
##          DODAG_PLC = nx.DiGraph()
##          DAGcreation(DAG_PLC, DODAG_PLC)
##          DODAG_RF = nx.DiGraph()
##          DAGcreation(DAG_RF, DODAG_RF)
##          dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
##          DODAG_RF_best = nx.DiGraph()
##          BestParent(DODAG_RF, DODAG_RF_best)
##          DODAG_PLC_best = nx.DiGraph()
##          BestParent(DODAG_PLC, DODAG_PLC_best)
##          graph_po_user = nx.Graph()
##          algoHybridPath_parent(graph_user, graph_po_user)
##          DODAG_PO = nx.DiGraph()
##          DAGcreation(graph_po_user, DODAG_PO)
##          DODAG_PO_best = nx.MultiDiGraph()
##          BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
##          DODAG_PO_Policy_best = nx.DiGraph()
##          policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
##          DODAG_IO = nx.DiGraph()
##          DAGcreation2(graph_user, DODAG_IO)
##          DODAG_IO_best = nx.DiGraph()
##          BestParent(DODAG_IO, DODAG_IO_best)
##          print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), "Case 400 : ", i)
##          case400.writerow([i+1, node_number, link_Q_rate_Multi(graph_user), link_Q_rate(DODAG_PO_Policy_best), link_Q_rate(DODAG_IO_best), parent_rate(DODAG_PO), parent_rate(DODAG_IO), average_hop(DODAG_PO_Policy_best)['hops'], average_hop(DODAG_PO_Policy_best)['min_hops'], average_hop(DODAG_PO_Policy_best)['max_hops'], average_hop(DODAG_IO_best)['hops'], average_hop(DODAG_IO_best)['min_hops'], average_hop(DODAG_IO_best)['max_hops'], rank_avr(DODAG_PO_Policy_best)['avr_rank'], rank_avr(DODAG_PO_Policy_best)['min_rank'], rank_avr(DODAG_PO_Policy_best)['max_rank'], rank_avr(DODAG_IO_best)['avr_rank'], rank_avr(DODAG_IO_best)['min_rank'], rank_avr(DODAG_IO_best)['max_rank'], test_DAG_MI(DAG_Hybrid)]) 
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##with open("stats500.csv", "wb") as file:
##     case500 = csv.writer(file)
##     case500.writerow(["Case","Node_NB", "LQ_Original","LQ_PO","LQ_IO","PP_PO","PP_IO","AVR_HOPS_PO","MIN_HOPS_PO","MAX_HOPS_PO","AVR_HOPS_IO","MIN_HOPS_IO","MAX_HOPS_IO","AVR_RANK_PO","MIN_RANK_PO","MAX_RANK_PO","AVR_RANK_IO","MIN_RANK_IO","MAX_RANK_IO", "DAG_MI_OK"])
##
##     for i in range(100):
##          DAG_PLC = nx.Graph()
##          DAG_RF = nx.Graph()
##          DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
##          graph_user = nx.MultiGraph() # Undirected graph creation
##          graph_random_2(500)
##          two_dags(graph_user, DAG_PLC, DAG_RF)
##          DODAG_PLC = nx.DiGraph()
##          DAGcreation(DAG_PLC, DODAG_PLC)
##          DODAG_RF = nx.DiGraph()
##          DAGcreation(DAG_RF, DODAG_RF)
##          dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
##          DODAG_RF_best = nx.DiGraph()
##          BestParent(DODAG_RF, DODAG_RF_best)
##          DODAG_PLC_best = nx.DiGraph()
##          BestParent(DODAG_PLC, DODAG_PLC_best)
##          graph_po_user = nx.Graph()
##          algoHybridPath_parent(graph_user, graph_po_user)
##          DODAG_PO = nx.DiGraph()
##          DAGcreation(graph_po_user, DODAG_PO)
##          DODAG_PO_best = nx.MultiDiGraph()
##          BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
##          DODAG_PO_Policy_best = nx.DiGraph()
##          policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
##          DODAG_IO = nx.DiGraph()
##          DAGcreation2(graph_user, DODAG_IO)
##          DODAG_IO_best = nx.DiGraph()
##          BestParent(DODAG_IO, DODAG_IO_best)
##          print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), "Case 500 : ", i)
##          case500.writerow([i+1, node_number, link_Q_rate_Multi(graph_user), link_Q_rate(DODAG_PO_Policy_best), link_Q_rate(DODAG_IO_best), parent_rate(DODAG_PO), parent_rate(DODAG_IO), average_hop(DODAG_PO_Policy_best)['hops'], average_hop(DODAG_PO_Policy_best)['min_hops'], average_hop(DODAG_PO_Policy_best)['max_hops'], average_hop(DODAG_IO_best)['hops'], average_hop(DODAG_IO_best)['min_hops'], average_hop(DODAG_IO_best)['max_hops'], rank_avr(DODAG_PO_Policy_best)['avr_rank'], rank_avr(DODAG_PO_Policy_best)['min_rank'], rank_avr(DODAG_PO_Policy_best)['max_rank'], rank_avr(DODAG_IO_best)['avr_rank'], rank_avr(DODAG_IO_best)['min_rank'], rank_avr(DODAG_IO_best)['max_rank'], test_DAG_MI(DAG_Hybrid)]) 
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##with open("stats600.csv", "wb") as file:
##     case600 = csv.writer(file)
##     case600.writerow(["Case","Node_NB", "LQ_Original","LQ_PO","LQ_IO","PP_PO","PP_IO","AVR_HOPS_PO","MIN_HOPS_PO","MAX_HOPS_PO","AVR_HOPS_IO","MIN_HOPS_IO","MAX_HOPS_IO","AVR_RANK_PO","MIN_RANK_PO","MAX_RANK_PO","AVR_RANK_IO","MIN_RANK_IO","MAX_RANK_IO", "DAG_MI_OK"])
##
##     for i in range(100):
##          DAG_PLC = nx.Graph()
##          DAG_RF = nx.Graph()
##          DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
##          graph_user = nx.MultiGraph() # Undirected graph creation
##          graph_random_2(600)
##          two_dags(graph_user, DAG_PLC, DAG_RF)
##          DODAG_PLC = nx.DiGraph()
##          DAGcreation(DAG_PLC, DODAG_PLC)
##          DODAG_RF = nx.DiGraph()
##          DAGcreation(DAG_RF, DODAG_RF)
##          dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
##          DODAG_RF_best = nx.DiGraph()
##          BestParent(DODAG_RF, DODAG_RF_best)
##          DODAG_PLC_best = nx.DiGraph()
##          BestParent(DODAG_PLC, DODAG_PLC_best)
##          graph_po_user = nx.Graph()
##          algoHybridPath_parent(graph_user, graph_po_user)
##          DODAG_PO = nx.DiGraph()
##          DAGcreation(graph_po_user, DODAG_PO)
##          DODAG_PO_best = nx.MultiDiGraph()
##          BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
##          DODAG_PO_Policy_best = nx.DiGraph()
##          policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
##          DODAG_IO = nx.DiGraph()
##          DAGcreation2(graph_user, DODAG_IO)
##          DODAG_IO_best = nx.DiGraph()
##          BestParent(DODAG_IO, DODAG_IO_best)
##          print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), "Case 600 : ", i)
##          case600.writerow([i+1, node_number, link_Q_rate_Multi(graph_user), link_Q_rate(DODAG_PO_Policy_best), link_Q_rate(DODAG_IO_best), parent_rate(DODAG_PO), parent_rate(DODAG_IO), average_hop(DODAG_PO_Policy_best)['hops'], average_hop(DODAG_PO_Policy_best)['min_hops'], average_hop(DODAG_PO_Policy_best)['max_hops'], average_hop(DODAG_IO_best)['hops'], average_hop(DODAG_IO_best)['min_hops'], average_hop(DODAG_IO_best)['max_hops'], rank_avr(DODAG_PO_Policy_best)['avr_rank'], rank_avr(DODAG_PO_Policy_best)['min_rank'], rank_avr(DODAG_PO_Policy_best)['max_rank'], rank_avr(DODAG_IO_best)['avr_rank'], rank_avr(DODAG_IO_best)['min_rank'], rank_avr(DODAG_IO_best)['max_rank'], test_DAG_MI(DAG_Hybrid)]) 
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##with open("stats700.csv", "wb") as file:
##     case700 = csv.writer(file)
##     case700.writerow(["Case","Node_NB", "LQ_Original","LQ_PO","LQ_IO","PP_PO","PP_IO","AVR_HOPS_PO","MIN_HOPS_PO","MAX_HOPS_PO","AVR_HOPS_IO","MIN_HOPS_IO","MAX_HOPS_IO","AVR_RANK_PO","MIN_RANK_PO","MAX_RANK_PO","AVR_RANK_IO","MIN_RANK_IO","MAX_RANK_IO", "DAG_MI_OK"])
##
##     for i in range(100):
##          DAG_PLC = nx.Graph()
##          DAG_RF = nx.Graph()
##          DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
##          graph_user = nx.MultiGraph() # Undirected graph creation
##          graph_random_2(700)
##          two_dags(graph_user, DAG_PLC, DAG_RF)
##          DODAG_PLC = nx.DiGraph()
##          DAGcreation(DAG_PLC, DODAG_PLC)
##          DODAG_RF = nx.DiGraph()
##          DAGcreation(DAG_RF, DODAG_RF)
##          dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
##          DODAG_RF_best = nx.DiGraph()
##          BestParent(DODAG_RF, DODAG_RF_best)
##          DODAG_PLC_best = nx.DiGraph()
##          BestParent(DODAG_PLC, DODAG_PLC_best)
##          graph_po_user = nx.Graph()
##          algoHybridPath_parent(graph_user, graph_po_user)
##          DODAG_PO = nx.DiGraph()
##          DAGcreation(graph_po_user, DODAG_PO)
##          DODAG_PO_best = nx.MultiDiGraph()
##          BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
##          DODAG_PO_Policy_best = nx.DiGraph()
##          policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
##          DODAG_IO = nx.DiGraph()
##          DAGcreation2(graph_user, DODAG_IO)
##          DODAG_IO_best = nx.DiGraph()
##          BestParent(DODAG_IO, DODAG_IO_best)
##          print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), "Case 700 : ", i)
##          case700.writerow([i+1, node_number, link_Q_rate_Multi(graph_user), link_Q_rate(DODAG_PO_Policy_best), link_Q_rate(DODAG_IO_best), parent_rate(DODAG_PO), parent_rate(DODAG_IO), average_hop(DODAG_PO_Policy_best)['hops'], average_hop(DODAG_PO_Policy_best)['min_hops'], average_hop(DODAG_PO_Policy_best)['max_hops'], average_hop(DODAG_IO_best)['hops'], average_hop(DODAG_IO_best)['min_hops'], average_hop(DODAG_IO_best)['max_hops'], rank_avr(DODAG_PO_Policy_best)['avr_rank'], rank_avr(DODAG_PO_Policy_best)['min_rank'], rank_avr(DODAG_PO_Policy_best)['max_rank'], rank_avr(DODAG_IO_best)['avr_rank'], rank_avr(DODAG_IO_best)['min_rank'], rank_avr(DODAG_IO_best)['max_rank'], test_DAG_MI(DAG_Hybrid)]) 
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##with open("stats800.csv", "wb") as file:
##     case800 = csv.writer(file)
##     case800.writerow(["Case","Node_NB", "LQ_Original","LQ_PO","LQ_IO","PP_PO","PP_IO","AVR_HOPS_PO","MIN_HOPS_PO","MAX_HOPS_PO","AVR_HOPS_IO","MIN_HOPS_IO","MAX_HOPS_IO","AVR_RANK_PO","MIN_RANK_PO","MAX_RANK_PO","AVR_RANK_IO","MIN_RANK_IO","MAX_RANK_IO", "DAG_MI_OK"])
##
##     for i in range(100):
##          DAG_PLC = nx.Graph()
##          DAG_RF = nx.Graph()
##          DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
##          graph_user = nx.MultiGraph() # Undirected graph creation
##          graph_random_2(800)
##          two_dags(graph_user, DAG_PLC, DAG_RF)
##          DODAG_PLC = nx.DiGraph()
##          DAGcreation(DAG_PLC, DODAG_PLC)
##          DODAG_RF = nx.DiGraph()
##          DAGcreation(DAG_RF, DODAG_RF)
##          dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
##          DODAG_RF_best = nx.DiGraph()
##          BestParent(DODAG_RF, DODAG_RF_best)
##          DODAG_PLC_best = nx.DiGraph()
##          BestParent(DODAG_PLC, DODAG_PLC_best)
##          graph_po_user = nx.Graph()
##          algoHybridPath_parent(graph_user, graph_po_user)
##          DODAG_PO = nx.DiGraph()
##          DAGcreation(graph_po_user, DODAG_PO)
##          DODAG_PO_best = nx.MultiDiGraph()
##          BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
##          DODAG_PO_Policy_best = nx.DiGraph()
##          policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
##          DODAG_IO = nx.DiGraph()
##          DAGcreation2(graph_user, DODAG_IO)
##          DODAG_IO_best = nx.DiGraph()
##          BestParent(DODAG_IO, DODAG_IO_best)
##          print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), "Case 800 : ", i)
##          case800.writerow([i+1, node_number, link_Q_rate_Multi(graph_user), link_Q_rate(DODAG_PO_Policy_best), link_Q_rate(DODAG_IO_best), parent_rate(DODAG_PO), parent_rate(DODAG_IO), average_hop(DODAG_PO_Policy_best)['hops'], average_hop(DODAG_PO_Policy_best)['min_hops'], average_hop(DODAG_PO_Policy_best)['max_hops'], average_hop(DODAG_IO_best)['hops'], average_hop(DODAG_IO_best)['min_hops'], average_hop(DODAG_IO_best)['max_hops'], rank_avr(DODAG_PO_Policy_best)['avr_rank'], rank_avr(DODAG_PO_Policy_best)['min_rank'], rank_avr(DODAG_PO_Policy_best)['max_rank'], rank_avr(DODAG_IO_best)['avr_rank'], rank_avr(DODAG_IO_best)['min_rank'], rank_avr(DODAG_IO_best)['max_rank'], test_DAG_MI(DAG_Hybrid)]) 
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##with open("stats900.csv", "wb") as file:
##     case900 = csv.writer(file)
##     case900.writerow(["Case","Node_NB", "LQ_Original","LQ_PO","LQ_IO","PP_PO","PP_IO","AVR_HOPS_PO","MIN_HOPS_PO","MAX_HOPS_PO","AVR_HOPS_IO","MIN_HOPS_IO","MAX_HOPS_IO","AVR_RANK_PO","MIN_RANK_PO","MAX_RANK_PO","AVR_RANK_IO","MIN_RANK_IO","MAX_RANK_IO", "DAG_MI_OK"])
##
##     for i in range(100):
##          DAG_PLC = nx.Graph()
##          DAG_RF = nx.Graph()
##          DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
##          graph_user = nx.MultiGraph() # Undirected graph creation
##          graph_random_2(900)
##          two_dags(graph_user, DAG_PLC, DAG_RF)
##          DODAG_PLC = nx.DiGraph()
##          DAGcreation(DAG_PLC, DODAG_PLC)
##          DODAG_RF = nx.DiGraph()
##          DAGcreation(DAG_RF, DODAG_RF)
##          dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
##          DODAG_RF_best = nx.DiGraph()
##          BestParent(DODAG_RF, DODAG_RF_best)
##          DODAG_PLC_best = nx.DiGraph()
##          BestParent(DODAG_PLC, DODAG_PLC_best)
##          graph_po_user = nx.Graph()
##          algoHybridPath_parent(graph_user, graph_po_user)
##          DODAG_PO = nx.DiGraph()
##          DAGcreation(graph_po_user, DODAG_PO)
##          DODAG_PO_best = nx.MultiDiGraph()
##          BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
##          DODAG_PO_Policy_best = nx.DiGraph()
##          policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
##          DODAG_IO = nx.DiGraph()
##          DAGcreation2(graph_user, DODAG_IO)
##          DODAG_IO_best = nx.DiGraph()
##          BestParent(DODAG_IO, DODAG_IO_best)
##          print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), "Case 900 : ", i)
##          case900.writerow([i+1, node_number, link_Q_rate_Multi(graph_user), link_Q_rate(DODAG_PO_Policy_best), link_Q_rate(DODAG_IO_best), parent_rate(DODAG_PO), parent_rate(DODAG_IO), average_hop(DODAG_PO_Policy_best)['hops'], average_hop(DODAG_PO_Policy_best)['min_hops'], average_hop(DODAG_PO_Policy_best)['max_hops'], average_hop(DODAG_IO_best)['hops'], average_hop(DODAG_IO_best)['min_hops'], average_hop(DODAG_IO_best)['max_hops'], rank_avr(DODAG_PO_Policy_best)['avr_rank'], rank_avr(DODAG_PO_Policy_best)['min_rank'], rank_avr(DODAG_PO_Policy_best)['max_rank'], rank_avr(DODAG_IO_best)['avr_rank'], rank_avr(DODAG_IO_best)['min_rank'], rank_avr(DODAG_IO_best)['max_rank'], test_DAG_MI(DAG_Hybrid)]) 
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##############################################################################
##with open("stats1000.csv", "wb") as file:
##     case1000 = csv.writer(file)
##     case1000.writerow(["Case","Node_NB", "LQ_Original","LQ_PO","LQ_IO","PP_PO","PP_IO","AVR_HOPS_PO","MIN_HOPS_PO","MAX_HOPS_PO","AVR_HOPS_IO","MIN_HOPS_IO","MAX_HOPS_IO","AVR_RANK_PO","MIN_RANK_PO","MAX_RANK_PO","AVR_RANK_IO","MIN_RANK_IO","MAX_RANK_IO", "DAG_MI_OK"])
##
##     for i in range(100):
##          DAG_PLC = nx.Graph()
##          DAG_RF = nx.Graph()
##          DAG_Hybrid = nx.MultiDiGraph()#Empty Multi Di Graph creation
##          graph_user = nx.MultiGraph() # Undirected graph creation
##          graph_random_2(1000)
##          two_dags(graph_user, DAG_PLC, DAG_RF)
##          DODAG_PLC = nx.DiGraph()
##          DAGcreation(DAG_PLC, DODAG_PLC)
##          DODAG_RF = nx.DiGraph()
##          DAGcreation(DAG_RF, DODAG_RF)
##          dags_to_multigraph(DODAG_RF, DODAG_PLC, DAG_Hybrid) 
##          DODAG_RF_best = nx.DiGraph()
##          BestParent(DODAG_RF, DODAG_RF_best)
##          DODAG_PLC_best = nx.DiGraph()
##          BestParent(DODAG_PLC, DODAG_PLC_best)
##          graph_po_user = nx.Graph()
##          algoHybridPath_parent(graph_user, graph_po_user)
##          DODAG_PO = nx.DiGraph()
##          DAGcreation(graph_po_user, DODAG_PO)
##          DODAG_PO_best = nx.MultiDiGraph()
##          BestParent_PO(DODAG_PO, DODAG_PO_best, graph_user )
##          DODAG_PO_Policy_best = nx.DiGraph()
##          policy_PO(DODAG_PO_best, DODAG_PO_Policy_best)
##          DODAG_IO = nx.DiGraph()
##          DAGcreation2(graph_user, DODAG_IO)
##          DODAG_IO_best = nx.DiGraph()
##          BestParent(DODAG_IO, DODAG_IO_best)
##          print(datetime.now().strftime('%Y-%m-%d %H:%M:%S'), "Case 1000 : ", i)
##          case1000.writerow([i+1, node_number, link_Q_rate_Multi(graph_user), link_Q_rate(DODAG_PO_Policy_best), link_Q_rate(DODAG_IO_best), parent_rate(DODAG_PO), parent_rate(DODAG_IO), average_hop(DODAG_PO_Policy_best)['hops'], average_hop(DODAG_PO_Policy_best)['min_hops'], average_hop(DODAG_PO_Policy_best)['max_hops'], average_hop(DODAG_IO_best)['hops'], average_hop(DODAG_IO_best)['min_hops'], average_hop(DODAG_IO_best)['max_hops'], rank_avr(DODAG_PO_Policy_best)['avr_rank'], rank_avr(DODAG_PO_Policy_best)['min_rank'], rank_avr(DODAG_PO_Policy_best)['max_rank'], rank_avr(DODAG_IO_best)['avr_rank'], rank_avr(DODAG_IO_best)['min_rank'], rank_avr(DODAG_IO_best)['max_rank'], test_DAG_MI(DAG_Hybrid)]) 
##############################################################################
##############################################################################
##############################################################################
import cProfile
       
for w in range(10):
     cProfile.run("simul(w)")
